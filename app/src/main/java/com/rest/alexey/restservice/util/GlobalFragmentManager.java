package com.rest.alexey.restservice.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.rest.alexey.restservice.R;

/**
 * Клас для управління фрагментами
 */

public class GlobalFragmentManager {

    //поточний фрагмент
    private static Fragment currentFragment;

    private GlobalFragmentManager() {}

    //отримати поточний фрагмент
    public static Fragment getCurrentFragment() {
        return currentFragment;
    }

    //встановити поточний фрагмент
    public static void setCurrentFragment(final Fragment currentFragment) {
        GlobalFragmentManager.currentFragment = currentFragment;
    }

    //отримати FragmentManager
    private static FragmentManager getFragmentManager(final FragmentActivity activity) {
        return activity.getSupportFragmentManager();
    }

    //очистити BackStack
    public static void clearBackStack(final FragmentActivity activity) {
        FragmentManager manager = getFragmentManager(activity);

        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    //встановити попередный фрагмент
    public static void popBackStack(int num, final FragmentActivity activity) {
        FragmentManager manager = getFragmentManager(activity);

        for(int i = 0; i < num; i++) {
            manager.popBackStack();
        }
    }

    //замінити поточний фрагмент
    public static void replace(final Fragment fragment, final FragmentActivity activity) {
        FragmentManager manager = getFragmentManager(activity);

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.main_fragment_container, fragment, fragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = fragment;
    }

    //відобразити поточний фрагмент
    public static void replaceOnCurrent(final FragmentActivity activity) {
        checkCurrentFragment();
        replace(currentFragment, activity);
    }

    //замінити поточний фрагмент з передачею даних
    public static void replaceWithBundle(final Fragment fragment, final Bundle bundle, final FragmentActivity activity) {
        fragment.setArguments(bundle);
        replace(fragment, activity);
    }

    //замінити поточний фрагмент з додавання до BackStack
    public static void replaceAndAddToBackStack(final Fragment fragment, final FragmentActivity activity) {
        FragmentManager manager = getFragmentManager(activity);

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.main_fragment_container, fragment, fragment.getClass().getName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = fragment;
    }

    //замінити поточний фрагмент з передачею даних та додаванням до BackStack
    public static void replaceAndAddToBackStackWithBundle(final Fragment fragment, final Bundle bundle,
                                                   final FragmentActivity activity) {
        fragment.setArguments(bundle);
        replaceAndAddToBackStack(fragment, activity);
    }

    //показати поточний фрагмент
    public static void showCurrent(final FragmentActivity activity) {
        FragmentManager manager = getFragmentManager(activity);

        checkCurrentFragment();

        manager.beginTransaction()
                .show(currentFragment)
                .commit();
    }

    //приховати поточний фрагмент
    public static void hideCurrent(final FragmentActivity activity) {
        FragmentManager manager = getFragmentManager(activity);

        if (currentFragment == null) {
            throw new NullPointerException("currentFragment is null");
        }

        manager.beginTransaction()
                .hide(currentFragment)
                .commit();
    }

    //перевірити коректність поточного фрагменту
    private static void checkCurrentFragment() {
        if (currentFragment == null) {
            throw new NullPointerException("currentFragment is null");
        }
    }
}
