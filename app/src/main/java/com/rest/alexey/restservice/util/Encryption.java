package com.rest.alexey.restservice.util;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Клас для шифрування даних
 */
public class Encryption {

    //шифрує строку за алгоритмом MD5
    public static String encryptMD5(final String data) {
        byte[] encryptedData = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            encryptedData = md5.digest(data.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return toHexString(encryptedData);
    }

    //приводить масив байтів у строкове представлення
    private static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }
}
