package com.rest.alexey.restservice.controller.Messenger;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.entity.ConversationEntity;
import com.rest.alexey.restservice.model.entity.MessageEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.ConversationREST;
import com.rest.alexey.restservice.model.rest.impl.MessageREST;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;

/**
 * Клас фрагменту для відображення розмов
 */

public class ConversationListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ConversationItemAdapter mAdapter;

    //обробник створення фрагменту
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //need to get data
        new GetConversationsAndUpdateUI().execute();

        return view;
    }

    //обробник створення меню
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    //обробник натискання на пункт меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //додавання
            case R.id.menu_action_add:
                GlobalFragmentManager.replaceAndAddToBackStack(new AddConversationFragment(), getActivity());
                return true;

            //оновлення
            case R.id.menu_action_update:
                new GetConversationsAndUpdateUI().execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //оновлення інтерфейсу
    private void updateUI(List<ConversationEntity> conversations, List<MessageEntity> lastMessages) {
        mAdapter = new ConversationItemAdapter(conversations, lastMessages);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Клас для адаптування розмов до представлення
     */

    private class ConversationItemAdapter extends RecyclerView.Adapter<ConversationHolder> {
        private List<ConversationEntity> mConversations;
        private List<MessageEntity> mLastMessages;

        public ConversationItemAdapter(List<ConversationEntity> conversations, List<MessageEntity> lastMessages) {
            this.mConversations = conversations;
            this.mLastMessages = lastMessages;
        }

        @Override
        public ConversationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_conversation, parent, false);
            return new ConversationHolder(view);
        }

        //зв'язування представлення
        @Override
        public void onBindViewHolder(ConversationHolder holder, int position) {
            ConversationEntity conversation = mConversations.get(position);
            MessageEntity lastMessage = mLastMessages.get(position);
            holder.bind(conversation, lastMessage);
        }

        @Override
        public int getItemCount() {
            return mConversations.size();
        }
    }

    /**
     * Клас для отримання розмов та оновлення інтерфейсу
     */

    private class GetConversationsAndUpdateUI extends AsyncTask<Void, Void, REST.Result> {

        private ConversationREST conversationREST = new ConversationREST();
        private MessageREST messageREST = new MessageREST();

        private List<ConversationEntity> conversations;
        private List<MessageEntity> lastMessages;

        @Override
        protected REST.Result doInBackground(Void... params) {

            REST.Result result;

            //отримання розмов
            try {
                conversations = conversationREST.getAllPersonal();

                if (conversations.size() > 0) {
                    lastMessages = getLastMessages();
                }

                result = new REST.Result(REST.AccessResult.SUCCESSFUL);

            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        //отримання останніх повідомлень
        private List<MessageEntity> getLastMessages() {
            List<Long> conversationIds = new ArrayList<>();

            for (ConversationEntity con : conversations) {
                conversationIds.add(con.getId());
            }

            return messageREST.getPersonalLastMessages(conversationIds);
        }

        //оновлення інтерфейсу
        @Override
        protected void onPostExecute(REST.Result result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                updateUI(conversations, lastMessages);
            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)) {
                Toast.makeText(getActivity(), R.string.access_denied,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getActivity(), R.string.cannot_connect,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
