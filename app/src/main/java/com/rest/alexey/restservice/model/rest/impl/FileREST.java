package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.FileDTO;
import com.rest.alexey.restservice.model.rest.REST;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class FileREST extends REST<Object> {
    public FileREST() {
        super("files/");
    }

    public boolean upload(FileDTO file) {
        HttpEntity<FileDTO> request = new HttpEntity<>(file, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + "upload/", HttpMethod.POST, request,
                new ParameterizedTypeReference<Boolean>() {});

        return response.getBody();
    }

    public FileDTO download(String namePath) {
        HttpEntity<String> request = new HttpEntity<>(namePath, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<FileDTO> response = sRestTemplate.exchange(sURL + "download/", HttpMethod.POST, request,
                new ParameterizedTypeReference<FileDTO>() {
                });
        return response.getBody();
    }

    @Override
    public long add(Object entity) {
        throw new RuntimeException("Not realized method");
    }

    @Override
    public boolean delete(long id) {
        throw new RuntimeException("Not realized method");
    }

    @Override
    public AccountEntity get(long id) {
        throw new RuntimeException("Not realized method");
    }

    @Override
    public List<Object> getAll() {
        throw new RuntimeException("Not realized method");
    }

    @Override
    public boolean update(long id, Object entity) {
        throw new RuntimeException("Not realized method");
    }
}
