package com.rest.alexey.restservice.model.entity;

import java.util.Date;

/**
 * Клас сутності повідомлення
 */

public class MessageEntity extends AbstractEntity {

    private ConversationEntity conversation;
    private UserEntity author;
    private String message;
    private Date releaseDate;

    public MessageEntity() {}
    public MessageEntity(ConversationEntity conversation, UserEntity author, String message,
                         Date releaseDate) {
        this.conversation = conversation;
        this.author = author;
        this.message = message;
        this.releaseDate = releaseDate;
    }

    public ConversationEntity getConversation() {
        return conversation;
    }

    public void setConversation(ConversationEntity conversation) {
        this.conversation = conversation;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}