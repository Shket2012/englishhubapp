package com.rest.alexey.restservice.controller.Group;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.rest.impl.GroupREST;
import com.rest.alexey.restservice.model.task.AddTask;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Клас фрагменту для додавання групи
 */

public class AddGroupEntityFragment extends AbstractFragment {

    private EditText mNameEditText;
    private EditText mLevelEditText;
    private EditText mStartDateEditText;
    private EditText mEndDateEditText;
    private EditText mScheduleEditText;
    private EditText mTrainingMaterialsEditText;

    private Button mAddButton;

    private GroupEntity mEntity;

    //обробник на створення екземпляру
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mEntity = new GroupEntity();
    }

    //метод для пошуку віджетів
    @Override
    protected void findWidgets(final View view) {
        mNameEditText = (EditText) view.findViewById(R.id.add_group_name_edit_text);
        mLevelEditText = (EditText) view.findViewById(R.id.add_group_level_edit_text);
        mStartDateEditText = (EditText) view.findViewById(R.id.add_group_start_date_edit_text);
        mEndDateEditText = (EditText) view.findViewById(R.id.add_group_end_date_edit_text);
        mScheduleEditText = (EditText) view.findViewById(R.id.add_group_schedule_edit_text);
        mTrainingMaterialsEditText = (EditText) view.findViewById(R.id.add_group_training_materials_edit_text);

        mAddButton = (Button) view.findViewById(R.id.add_group_button);
    }

    //метод для налагодження віджетів
    @Override
    protected void setUpWidgets() {
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String receivedName = mNameEditText.getText().toString().trim();

                if (receivedName.equals("")) {
                    Toast.makeText(getActivity(), R.string.incorrect_data, Toast.LENGTH_SHORT).show();
                    return;
                }

                mEntity.setName(receivedName);

                DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
                Date startDate = null;
                Date endDate = null;

                try {
                    startDate = format.parse(mStartDateEditText.getText().toString());
                    endDate = format.parse(mEndDateEditText.getText().toString());

                    final int parsedLevel = Integer.valueOf(mLevelEditText.getText().toString().trim());
                    mEntity.setLevel(parsedLevel);

                } catch (ParseException e) {
                    Toast.makeText(getActivity(), R.string.incorrect_date, Toast.LENGTH_SHORT).show();
                    return;
                } catch (NumberFormatException exc) {
                    Toast.makeText(getActivity(), R.string.incorrect_data, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (startDate.compareTo(endDate) >= 0) {
                    Toast.makeText(getActivity(), R.string.incorrect_date, Toast.LENGTH_SHORT).show();
                    return;
                }

                mEntity.setStartDate(startDate);
                mEntity.setEndDate(endDate);

                mEntity.setSchedule(mScheduleEditText.getText().toString().trim());
                mEntity.setTrainingMaterials(mTrainingMaterialsEditText.getText().toString().trim());

                //запуск потоку додавання групи
                new AddTask<>(new GroupREST(), mEntity, getContext()).execute();
            }
        });
    }

    //встановлює макет
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_add_group;
    }
}
