package com.rest.alexey.restservice.controller.News;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.UriWrapper;
import com.rest.alexey.restservice.model.entity.NewsEntity;
import com.rest.alexey.restservice.model.rest.impl.NewsREST;
import com.rest.alexey.restservice.model.task.DeleteTask;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

public class NewsFragment extends AbstractFragment {

    private TextView mTitleTextView;
    private TextView mReleaseDateTextView;
    private TextView mContentTextView;

    private ImageView mImageView;

    private NewsEntity mNewsEntity;
    private Uri mImageUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNewsEntity = (NewsEntity) getArguments()
                .getSerializable(NewsEntity.class.getName());
        UriWrapper wrapper = (UriWrapper) getArguments().getSerializable("NewsImage");

        if (wrapper != null) {
            mImageUri = wrapper.getUri();
            getArguments().remove("NewsImage");
        }
    }

    @Override
    protected void findWidgets(View view) {
        mTitleTextView = (TextView)  view.findViewById(R.id.news_title_text_edit);
        mReleaseDateTextView = (TextView) view.findViewById(R.id.news_release_date_text_edit);
        mContentTextView = (TextView) view.findViewById(R.id.news_content_text_edit);

        mImageView = (ImageView) view.findViewById(R.id.news_image_view);
    }

    @Override
    protected void setUpWidgets() {
        setHasOptionsMenu(true);

        mTitleTextView.setText(mNewsEntity.getTitle());
        mReleaseDateTextView.setText(DateFormatter.formatByDateTime(mNewsEntity.getReleaseDate()));
        mContentTextView.setText(mNewsEntity.getContent());

        if (mImageUri != null) {
            mImageView.setImageURI(mImageUri);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_news;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (Authorization.getAuthorization().getAuthorizationData().isAdmin()) {
            inflater.inflate(R.menu.menu_full_information, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_action_edit:
                Bundle bundle = getArguments();
                bundle.putSerializable("NewsImage", new UriWrapper(mImageUri));

                GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new EditNewsEntityFragment(),
                        bundle, getActivity());
                return true;

            case R.id.menu_action_delete:
                new DeleteTask<NewsEntity, NewsREST>(new NewsREST(), mNewsEntity, getContext()).execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
