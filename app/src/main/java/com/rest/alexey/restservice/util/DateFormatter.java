package com.rest.alexey.restservice.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Статичний клас для форматування дати
 */

public class DateFormatter {
    public static String formatByDayMonthYear(final Date date) {
        return new SimpleDateFormat("dd.MM.yyyy").format(date);
    }

    public static String formatByDateTime(final Date date) {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(date);
    }

    public static String formatWithoutSeparators(final Date date) {
        return new SimpleDateFormat("ddMMyyyyHHmmss").format(date);
    }
}
