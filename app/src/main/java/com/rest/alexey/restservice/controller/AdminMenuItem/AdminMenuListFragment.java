package com.rest.alexey.restservice.controller.AdminMenuItem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.AdminMenuItem;
import com.rest.alexey.restservice.model.AdminMenuItemProvider;

import java.util.List;

/**
 * Клас фрагменту для відображення пунктів меню
 */

public class AdminMenuListFragment extends Fragment {
    private RecyclerView mAdminRecyclerView;
    private AdminMenuListFragment.AdminMenuItemAdapter mAdapter;

    //обробник при створенні екземпляру
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        mAdminRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mAdminRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //отримання пунктів меню
        List<AdminMenuItem> items = new AdminMenuItemProvider().getAdminMenuItems();

        //оновлення UI
        updateUI(items);

        return view;
    }

    //оновлює UI
    private void updateUI(List<AdminMenuItem> adminMenuItems) {
        mAdapter = new AdminMenuListFragment.AdminMenuItemAdapter(adminMenuItems);
        mAdminRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Клас для адаптації view
     */

    private class AdminMenuItemAdapter extends RecyclerView.Adapter<AdminMenuItemHolder> {
        private List<AdminMenuItem> mAdminMenuItems;

        public AdminMenuItemAdapter(List<AdminMenuItem> adminMenuItems) {
            this.mAdminMenuItems = adminMenuItems;
        }

        @Override
        public AdminMenuItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_admin, parent, false);
            return new AdminMenuItemHolder(view);
        }

        @Override
        public void onBindViewHolder(AdminMenuItemHolder holder, int position) {
            AdminMenuItem item = mAdminMenuItems.get(position);
            holder.bind(item);
        }

        @Override
        public int getItemCount() {
            return mAdminMenuItems.size();
        }
    }
}
