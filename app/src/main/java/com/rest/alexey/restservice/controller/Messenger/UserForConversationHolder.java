package com.rest.alexey.restservice.controller.Messenger;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.entity.ConversationEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.ConversationREST;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;

/**
 * Клас для утримання користувача
 */

public class UserForConversationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private UserEntity mUser;

    private TextView mLastNameTextView;
    private TextView mFirstNameTextView;
    private TextView mGroupNameTextView;

    private View mView;

    public UserForConversationHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mView = itemView;

        mLastNameTextView = (TextView) itemView.findViewById(R.id.list_item_user_last_name_text_view);
        mFirstNameTextView = (TextView) itemView.findViewById(R.id.list_item_user_first_name_text_view);
        mGroupNameTextView = (TextView) itemView.findViewById(R.id.list_item_user_group_text_view);
    }

    //налаштування представлення
    public void bind(final UserEntity item) {
        mUser = item;

        mLastNameTextView.setText(mUser.getLastName());
        mFirstNameTextView.setText(mUser.getFirstName());
    }

    //обробник натискання на користувача
    @Override
    public void onClick(View v) {
        UserEntity authorizedUser = Authorization.getAuthorization().getAuthorizationData().getUser();

        List<UserEntity> users = new ArrayList<>();
        users.add(authorizedUser);
        users.add(mUser);

        ConversationEntity conversation = new ConversationEntity(mUser.getLastName() + " " +
                authorizedUser.getLastName(), users);

        //додавання користувача
        new AddTask(conversation).execute();
    }

    /**
     * Клас для додавання користувача
     */

    protected class AddTask extends AsyncTask<Void, Void, REST.Result<Long>> {

        private ConversationREST conversationREST = new ConversationREST();
        private ConversationEntity conversationEntity;

        public AddTask(final ConversationEntity conversationEntity) {
            this.conversationEntity = conversationEntity;
        }

        @Override
        protected REST.Result<Long> doInBackground(Void... params) {
            REST.Result<Long> result;

            //додавання новини
            try {
                Long idEntity = conversationREST.add(conversationEntity);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, idEntity);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        //відкриття попереднього вікна
        @Override
        protected void onPostExecute(REST.Result<Long> longResult) {
            GlobalFragmentManager.popBackStack(1, (MainActivity) mView.getContext());
        }
    }
}
