package com.rest.alexey.restservice.controller.News;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.entity.FileDTO;
import com.rest.alexey.restservice.model.entity.NewsEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.FileREST;
import com.rest.alexey.restservice.model.rest.impl.NewsREST;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.FileUtils;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey on 11.04.2017.
 */

public class NewsListFragment extends Fragment {

    private RecyclerView mNewsRecyclerView;
    private NewsAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        setHasOptionsMenu(true);

        mNewsRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mNewsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        new GetListNewsAndUpdateUI().execute();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(Authorization.getAuthorization().getAuthorizationData().isAdmin()) {
            inflater.inflate(R.menu.menu_list, menu);
        } else {
            inflater.inflate(R.menu.menu_update, menu);
        }
            super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_add:
                GlobalFragmentManager.replaceAndAddToBackStack(new AddNewsEntityFragment(), getActivity());
                return true;
            case R.id.menu_action_update:
                new GetListNewsAndUpdateUI().execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateUI(List<NewsEntity> news, List<Uri> images) {
        mAdapter = new NewsAdapter(news, images);
        mNewsRecyclerView.setAdapter(mAdapter);
    }

    private class NewsAdapter extends RecyclerView.Adapter<NewsHolder> {
        private List<NewsEntity> mNewsList;
        private List<Uri> mImagesList;

        public NewsAdapter(List<NewsEntity> news, List<Uri> images) {
            this.mNewsList = news;
            this.mImagesList = images;
        }

        @Override
        public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_news, parent, false);
            return new NewsHolder(view);
        }
        @Override
        public void onBindViewHolder(NewsHolder holder, int position) {
            NewsEntity news = mNewsList.get(position);
            Uri imagePath = mImagesList.get(position);
            holder.bindNews(news, imagePath);
        }
        @Override
        public int getItemCount() {
            return mNewsList.size();
        }
    }

    private class GetListNewsAndUpdateUI extends AsyncTask<Void, Void, REST.Result> {
        private NewsREST mNewsREST = new NewsREST();
        private FileREST mFileREST = new FileREST();

        private List<NewsEntity> news;
        private List<Uri> images = new ArrayList<>();

        @Override
        protected REST.Result doInBackground(Void... params) {
            REST.Result result;

            try {
                news = mNewsREST.getAll();

                if (news.size() > 0) {
                    getImages();
                }

                result = new REST.Result(REST.AccessResult.SUCCESSFUL);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        private void getImages() {
            String imageName;

            for (int i = 0; i < news.size(); i++) {
                imageName = news.get(i).getImageName();

                if (imageName != null) {
                    FileDTO fileDTO = mFileREST.download(imageName);

                    if (fileDTO != null) {
                        try {
                            Uri uriImage = FileUtils.writeFile(fileDTO.getFile(), imageName);
                            images.add(uriImage);
                        } catch (IOException e) {
                            Log.w("NewsListFragment", "Cannot write file: " + imageName);
                            images.add(null);
                        }
                    } else {
                        images.add(null);
                    }

                } else {
                    images.add(null);
                }
            }
        }

        @Override
        protected void onPostExecute(REST.Result result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                updateUI(news, images);
            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)) {
                Toast.makeText(getActivity(), R.string.access_denied,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getActivity(), R.string.cannot_connect,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}

