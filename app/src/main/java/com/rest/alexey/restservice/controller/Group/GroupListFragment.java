package com.rest.alexey.restservice.controller.Group;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.GroupREST;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.List;

/**
 * Клас фрагменту для відображення списку груп
 */

public class GroupListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private GroupItemAdapter mAdapter;

    //обробник створення фрагменту
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //need to get data
        new GetGroupsAndUpdateUI().execute();

        return view;
    }

    //обробник створення меню
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //обробник натискання на пункт меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //додавання
            case R.id.menu_action_add:
                GlobalFragmentManager.replaceAndAddToBackStack(new AddGroupEntityFragment(), getActivity());
                return true;

            //оновлення
            case R.id.menu_action_update:
                new GetGroupsAndUpdateUI().execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //оновлення інтерфейсу
    private void updateUI(List<GroupEntity> items) {
        mAdapter = new GroupItemAdapter(items);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Клас для адаптування груп до відображення
     */

    private class GroupItemAdapter extends RecyclerView.Adapter<GroupHolder> {
        private List<GroupEntity> mItems;

        public GroupItemAdapter(List<GroupEntity> items) {
            this.mItems = items;
        }

        @Override
        public GroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_group, parent, false);
            return new GroupHolder(view);
        }

        @Override
        public void onBindViewHolder(GroupHolder holder, int position) {
            GroupEntity item = mItems.get(position);
            holder.bind(item);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    /**
     * Клас для отримання груп та оновлення інтерфейсу
     */

    private class GetGroupsAndUpdateUI extends AsyncTask<Void, Void, REST.Result<List<GroupEntity>>> {

        @Override
        protected REST.Result<List<GroupEntity>> doInBackground(Void... params) {
            GroupREST groupREST = new GroupREST();

            REST.Result<List<GroupEntity>> result;

            //отримання груп
            try {
                List<GroupEntity> users = groupREST.getAll();
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, users);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        //виконується після виконнання методу doInBackground()
        @Override
        protected void onPostExecute(REST.Result<List<GroupEntity>> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                updateUI(result.getResponse());
            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)) {
                Toast.makeText(GroupListFragment.this.getActivity(), R.string.access_denied,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(GroupListFragment.this.getActivity(), R.string.cannot_connect,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
