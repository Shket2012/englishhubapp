package com.rest.alexey.restservice.controller.News;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.UriWrapper;
import com.rest.alexey.restservice.model.entity.FileDTO;
import com.rest.alexey.restservice.model.entity.NewsEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.FileREST;
import com.rest.alexey.restservice.model.rest.impl.NewsREST;
import com.rest.alexey.restservice.model.task.EditTask;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.FileUtils;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * Клас фрагменту для редагування новини
 */

public class EditNewsEntityFragment extends AbstractFragment {

    private static final int SELECT_IMAGE = 100;
    private EditText mTitleEditText;
    private EditText mContentEditText;

    private ImageView mImageView;

    private Button mChooseImageButton;
    private Button mEditButton;

    private NewsEntity mNewsEntity;
    private Uri mImageUri;

    private FileDTO mFileDTO;
    private boolean mImageSelected;

    //отримання даних з попереднього фрагменту
    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);

        mNewsEntity = (NewsEntity) getArguments()
                .getSerializable(NewsEntity.class.getName());
        UriWrapper wrapper = (UriWrapper) getArguments().getSerializable("NewsImage");

        if (wrapper != null) {
            mImageUri = wrapper.getUri();
            getArguments().remove("NewsImage");
        }
    }

    //пошук віджетів
    @Override
    protected void findWidgets(View view) {
        mTitleEditText = (EditText)  view.findViewById(R.id.edit_news_title_edit_text);
        mContentEditText = (EditText) view.findViewById(R.id.edit_news_content_edit_text);

        mImageView = (ImageView) view.findViewById(R.id.edit_news_image_view);

        mChooseImageButton = (Button) view.findViewById(R.id.edit_news_choose_button);
        mEditButton = (Button) view.findViewById(R.id.edit_news_button);
    }

    //налаштування віджетів
    @Override
    protected void setUpWidgets() {

        mTitleEditText.setText(mNewsEntity.getTitle());
        mContentEditText.setText(mNewsEntity.getContent());

        mImageView.setImageURI(mImageUri);

        mChooseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = mTitleEditText.getText().toString();
                String content = mContentEditText.getText().toString();

                if (title.equals("") || content.equals("")) {
                    Toast.makeText(getActivity(), R.string.incorrect_data, Toast.LENGTH_SHORT).show();
                    return;
                }

                mNewsEntity.setTitle(title);
                mNewsEntity.setContent(content);

                if (mImageSelected) {
                    mNewsEntity.setImageName(mFileDTO.getName());
                    new UploadTask(mFileDTO).execute();

                    setImageNotSelected();
                }

                //редагування новини
                new EditTask<NewsEntity, NewsREST>(new NewsREST(), mNewsEntity, getContext()).execute();
            }
        });
    }

    //встановлення макету
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_news;
    }

    //відкриття галереї
    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, SELECT_IMAGE);
    }

    //відмічає зображення не обраним
    private void setImageSelected(Uri imageUri) {
        mImageView.setImageURI(imageUri);
        mImageSelected = true;
    }

    //відмічає зображення обраним
    private void setImageNotSelected() {
        mImageView.setImageURI(null);
        mImageSelected = false;
    }

    //обробка результату повернення з активності
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //якщо попередня активність - галерея
        if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {
            Uri imageUri = data.getData();

            String realPathImage = FileUtils.getRealPathFromURI(imageUri, getActivity());
            File imageFile = new File(realPathImage);

            if (imageFile.exists()) {

                byte[] imageInBytes = null;
                try {
                    imageInBytes = FileUtils.readFile(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mFileDTO = new FileDTO(DateFormatter.formatWithoutSeparators(new Date()) + ".png", imageInBytes);
                setImageSelected(imageUri);

            } else {
                Toast.makeText(getActivity(), "File doesn't exist", Toast.LENGTH_SHORT).show();
            }
        } else {
            setImageNotSelected();
        }
    }

    //завантаження файлу на сервер
    private class UploadTask extends AsyncTask<Void, Void, REST.Result<Boolean>> {

        private FileDTO mEntity;
        private FileREST mFileREST = new FileREST();

        public UploadTask(final FileDTO entity) {
            this.mEntity = entity;
        }

        @Override
        protected REST.Result<Boolean> doInBackground(Void... params) {
            REST.Result<Boolean> result;

            try {
                Boolean uploaded = mFileREST.upload(mEntity);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, uploaded);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<Boolean> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL) && result.getResponse()) {
                Toast.makeText(getActivity(), R.string.add_toast_successful,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getActivity(), R.string.add_toast_unsuccessful,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
