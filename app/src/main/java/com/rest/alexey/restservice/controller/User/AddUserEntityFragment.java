package com.rest.alexey.restservice.controller.User;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.model.rest.impl.GroupREST;
import com.rest.alexey.restservice.model.task.AddTask;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddUserEntityFragment extends AbstractFragment {

    private static final int layoutId = R.layout.fragment_add_user;

    private EditText mLastNameEditText;
    private EditText mFirstNameEditText;
    private EditText mMiddleNameEditText;
    private EditText mDateBirthEditText;

    private EditText mLoginEditText;
    private EditText mPasswordEditText;

    private Spinner mGroupSpinner;
    private CheckBox mAdminCheckBox;

    private Button mRegisterButton;

    private List<GroupEntity> mGroups;

    @Override
    protected void findWidgets(View view) {
        mLastNameEditText = (EditText) view.findViewById(R.id.add_user_last_name_edit_text);
        mFirstNameEditText = (EditText) view.findViewById(R.id.add_user_first_name_edit_text);
        mMiddleNameEditText = (EditText) view.findViewById(R.id.add_user_middle_name_edit_text);
        mDateBirthEditText = (EditText) view.findViewById(R.id.add_user_birth_date_edit_text);

        mGroupSpinner = (Spinner) view.findViewById(R.id.add_user_group_spinner);
        mAdminCheckBox = (CheckBox) view.findViewById(R.id.add_user_admin_check_box);

        mLoginEditText = (EditText) view.findViewById(R.id.add_user_login_edit_text);
        mPasswordEditText = (EditText) view.findViewById(R.id.add_user_password_edit_text);

        mRegisterButton = (Button) view.findViewById(R.id.register_user_button);
    }

    @Override
    protected void setUpWidgets() {
        new GetGroupsAndSetUpGroupSpinner().execute();

        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserEntity user = new UserEntity();
                user.setLastName(mLastNameEditText.getText().toString());
                user.setFirstName(mFirstNameEditText.getText().toString());
                user.setMiddleName(mMiddleNameEditText.getText().toString());

                DateFormat format = new SimpleDateFormat("ddd.mm.yyyy", Locale.ENGLISH);
                Date dateBirth = null;
                try {
                    dateBirth = format.parse(mDateBirthEditText.getText().toString());
                } catch (ParseException e) {
                    Toast.makeText(getActivity(), "Incorrect date !", Toast.LENGTH_SHORT).show();
                }

                if (dateBirth == null) {
                    return;
                }

                user.setBirthDate(dateBirth);

                int selectedPositionGroup = mGroupSpinner.getSelectedItemPosition();
                GroupEntity selectedGroup = mGroups.get(selectedPositionGroup);

                user.setGroup(selectedGroup);

                AccountEntity account = new AccountEntity();
                account.setLoginAndEncrypt(mLoginEditText.getText().toString());
                account.setPasswordAndEncrypt(mPasswordEditText.getText().toString());

                account.setAdmin(mAdminCheckBox.isChecked());

                account.setUser(user);

                new AddTask<>(new AccountREST(), account, getContext()).execute();
            }
        });
    }

    private void setUpGroupSpinner(final List<CharSequence> groups) {
        ArrayAdapter<CharSequence> groupAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, groups);

        mGroupSpinner.setAdapter(groupAdapter);
    }

    @Override
    protected int getLayoutId() {
        return layoutId;
    }

    private class GetGroupsAndSetUpGroupSpinner extends AsyncTask<Void, Void, REST.Result<List<GroupEntity>>> {

        private GroupREST groupREST = new GroupREST();

        @Override
        protected REST.Result<List<GroupEntity>>doInBackground(Void... params) {
            REST.Result<List<GroupEntity>> result;

            try {
                List<GroupEntity> groups = groupREST.getAll();
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, groups);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<List<GroupEntity>> result) {
            REST.AccessResult accessResult = result.getAccessResult();
            mGroups = result.getResponse();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL) && mGroups.size() > 0) {
                List<CharSequence> groupNames = new ArrayList<>();

                for (GroupEntity group : mGroups) {
                    groupNames.add(group.getName());
                }

                setUpGroupSpinner(groupNames);
            } else {
                mRegisterButton.setEnabled(false);
            }
        }
    }
}
