package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.entity.NewsEntity;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Alexey on 11.04.2017.
 */

public class NewsREST extends REST <NewsEntity> {
    public NewsREST() {
        super("news/");
    }

    public long add(NewsEntity entity) {
        HttpEntity<NewsEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<NewsEntity> response = sRestTemplate.exchange(sURL, HttpMethod.POST, request,
                new ParameterizedTypeReference<NewsEntity>() {});

        return response.getBody().getId();
    }

    public boolean delete(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {});
        return response.getBody();
    }

    public NewsEntity get(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<NewsEntity> response = sRestTemplate.exchange(sURL + id, HttpMethod.GET, request,
                new ParameterizedTypeReference<NewsEntity>() {});
        return response.getBody();
    }

    public List<NewsEntity> getAll() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<NewsEntity>> response = sRestTemplate.exchange(sURL, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<NewsEntity>>() {});
        return response.getBody();
    }

    public boolean update(long id, NewsEntity entity) {
        HttpEntity<NewsEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {});
        return response.getBody();
    }
}
