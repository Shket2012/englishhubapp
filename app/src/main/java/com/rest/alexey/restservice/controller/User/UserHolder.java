package com.rest.alexey.restservice.controller.User;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.util.GlobalFragmentManager;


public class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private AccountEntity mAccount;

    private TextView mLastNameTextView;
    private TextView mFirstNameTextView;
    private TextView mGroupNameTextView;

    public UserHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mLastNameTextView = (TextView) itemView.findViewById(R.id.list_item_user_last_name_text_view);
        mFirstNameTextView = (TextView) itemView.findViewById(R.id.list_item_user_first_name_text_view);
        mGroupNameTextView = (TextView) itemView.findViewById(R.id.list_item_user_group_text_view);
    }

    public void bind(final AccountEntity item) {
        mAccount = item;
        UserEntity user = mAccount.getUser();

        mLastNameTextView.setText(user.getLastName());
        mFirstNameTextView.setText(user.getFirstName());

        if (user.getGroup() != null) {
            mGroupNameTextView.setText(user.getGroup().getName());
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(mAccount.getClass().getName(), mAccount);

        GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new UserFragment(),
                bundle, (MainActivity) v.getContext());
    }
}
