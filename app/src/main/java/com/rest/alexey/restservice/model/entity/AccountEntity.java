package com.rest.alexey.restservice.model.entity;

import com.rest.alexey.restservice.util.Encryption;

import java.io.Serializable;

/**
 * Клас сутності аккаунт
 */

public class AccountEntity extends AbstractEntity implements Serializable {

    private UserEntity user;

    private String encryptedLogin;
    private String encryptedPassword;

    private boolean admin;

    public AccountEntity() {}

    public AccountEntity(final String login, final String password) {
        this.encryptedLogin = Encryption.encryptMD5(login);
        this.encryptedPassword = Encryption.encryptMD5(password);
    }

    public AccountEntity(final String login, final String password, boolean isAdmin) {
        this(login, password);
        admin = isAdmin;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getEncryptedLogin() {
        return encryptedLogin;
    }

    public void setLoginAndEncrypt(String login) {
        this.encryptedLogin = Encryption.encryptMD5(login);
    }

    public void setEncryptedLogin(String encryptedLogin) {
        this.encryptedLogin = encryptedLogin;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setPasswordAndEncrypt(String password) {
        this.encryptedPassword = Encryption.encryptMD5(password);
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
