package com.rest.alexey.restservice.model.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.entity.AbstractEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

/**
 * Клас для виконання запиту видалення у новому потоці
 */

public class DeleteTask<TypeEntity extends AbstractEntity, TypeDAO extends REST<TypeEntity>>
        extends AsyncTask<Void, Void, REST.Result<Boolean>> {

    private TypeEntity mEntity;
    private TypeDAO mEntityDAO;
    private Context mContext;

    public DeleteTask(final TypeDAO entityDAO, final TypeEntity entity, final Context context) {
        this.mEntity = entity;
        this.mEntityDAO = entityDAO;
        this.mContext = context;
    }

    @Override
    protected REST.Result<Boolean> doInBackground(Void... params) {
        REST.Result<Boolean> result;

        try {
            Boolean success = mEntityDAO.delete(mEntity.getId());
            result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, success);
        } catch (RestClientException exc) {
            if (exc instanceof ResourceAccessException) {
                result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
            } else {
                result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(REST.Result<Boolean> result) {
        REST.AccessResult accessResult = result.getAccessResult();

        if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
            Toast.makeText(mContext, R.string.delete_toast_successful,
                    Toast.LENGTH_SHORT)
                    .show();
            GlobalFragmentManager.popBackStack(2, (MainActivity) mContext);
        } else {
            Toast.makeText(mContext, R.string.delete_toast_unsuccessful,
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }
}
