package com.rest.alexey.restservice.controller.AdminMenuItem;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.AdminMenuItem;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

/**
 * Клас для утримання пункту меню
 */

public class AdminMenuItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private AdminMenuItem mAdminMenuItem;

    private TextView mTitleTextView;

    public AdminMenuItemHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_admin_title_text_view);
    }

    public void bind(AdminMenuItem item) {
        mAdminMenuItem = item;
        mTitleTextView.setText(item.getTitle());
    }

    //обробник натискання на пункт меню
    @Override
    public void onClick(View v) {
        Fragment fragment = mAdminMenuItem.getFragment();

        GlobalFragmentManager.replaceAndAddToBackStack(fragment, (MainActivity) v.getContext());
    }
}
