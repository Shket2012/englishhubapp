package com.rest.alexey.restservice.model.entity;

import java.util.List;

/**
 * Клас сутності розмова
 */

public class ConversationEntity extends AbstractEntity {

    private List<UserEntity> users;
    private String title;

    public ConversationEntity() {}
    public ConversationEntity(final String title, final List<UserEntity> users) {
        this.users = users;
        this.title = title;
    }

    public List<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}