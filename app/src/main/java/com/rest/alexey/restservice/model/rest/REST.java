package com.rest.alexey.restservice.model.rest;

import com.rest.alexey.restservice.util.AppServerURL;
import com.rest.alexey.restservice.util.Authorization;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Клас з частковою реалізацією передачі даних за технологією REST
 */

public abstract class REST<T> implements CRUD<T> {
    //Клас для утримання результату запиту
    public static class Result<T> {

        private AccessResult accessResult;
        private T response;

        public Result(){}

        public Result(final AccessResult accessResult) {
            this.accessResult = accessResult;
        }

        public Result(final AccessResult accessResult, final T response) {
            this.accessResult = accessResult;
            this.response = response;
        }

        public AccessResult getAccessResult() {
            return accessResult;
        }

        public void setAccessResult(AccessResult accessResult) {
            this.accessResult = accessResult;
        }

        public T getResponse() {
            return response;
        }

        public void setResponse(T response) {
            this.response = response;
        }
    }
    //перерахування для статусу результату
    public enum AccessResult {SUCCESSFUL, UNSUCCESSFUL, CANNOT_CONNECT}

    protected RestTemplate sRestTemplate;

    //адрес серверу
    protected String sURL = AppServerURL.get().getURL();
    protected Authorization sAuthorization = Authorization.getAuthorization();

    public REST(final String postfixUrl) {
        prepareRestTemplate();
        this.sURL += postfixUrl;
    }

    //налаштувати RestTemplate
    private void prepareRestTemplate() {
        sRestTemplate = new RestTemplate();
        sRestTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
    }


}
