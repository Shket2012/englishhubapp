package com.rest.alexey.restservice.controller.Messenger;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.entity.ConversationEntity;
import com.rest.alexey.restservice.model.entity.MessageEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

/**
 * Клас для утримання розмови
 */

public class ConversationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ConversationEntity mConversation;

    private TextView mDateTextView;
    private TextView mAuthorTextView;
    private TextView mLastMessageTextView;

    private TextView mTitleTextView;

    public ConversationHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mDateTextView = (TextView) itemView.findViewById(R.id.list_item_messenger_date_text_view);
        mLastMessageTextView = (TextView) itemView.findViewById(R.id.list_item_messenger_last_message_text_view);
        mAuthorTextView = (TextView) itemView.findViewById(R.id.list_item_messenger_author_text_view);

        mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_messenger_title_text_view);
    }

    //налагодження представлення
    public void bind(final ConversationEntity conversation, final MessageEntity lastMessage) {
        mConversation = conversation;

        if (lastMessage != null) {
            mDateTextView.setText(DateFormatter.formatByDateTime(lastMessage.getReleaseDate()));
            mLastMessageTextView.setText(lastMessage.getMessage());

            UserEntity author = lastMessage.getAuthor();

            mAuthorTextView.setText(author.getLastName() + " " + author.getFirstName());
        }

        mTitleTextView.setText(mConversation.getTitle());
    }

    //обробник натискання на новину
    @Override
    public void onClick(View v) {

        Bundle bundle = new Bundle();
        bundle.putLong("idConversation", mConversation.getId());

        GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new MessageListFragment(),
                bundle, (MainActivity) v.getContext());
    }
}
