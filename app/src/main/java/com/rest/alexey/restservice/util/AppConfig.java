package com.rest.alexey.restservice.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.rest.alexey.restservice.model.entity.AccountEntity;

/**
 * Клас для роботи з файлом конфігурації
 */

public final class AppConfig {
    //ім'я файлу конфігурації
    private static final String CONFIG_FILE_NAME = "shared_config";

    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String URL = "url";

    private final SharedPreferences mStorage;
    private final SharedPreferences.Editor mEditor;

    public AppConfig(final Context context) {
        mStorage = context.getSharedPreferences(CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        mEditor = mStorage.edit();
    }

    //зберегти дані авторизації
    public void storeAuthorizationData(final AccountEntity authorizationData) {
        mEditor.putString(LOGIN, authorizationData.getEncryptedLogin());
        mEditor.putString(PASSWORD, authorizationData.getEncryptedPassword());
        mEditor.apply();
    }

    public void storeURL(String url) {
        mEditor.putString(URL, "http://" + url + ":8080/");
        mEditor.apply();
    }

    //видалити файли конфігурації
    public void removeAuthorizationData() {
        mEditor.remove(LOGIN);
        mEditor.remove(PASSWORD);
        mEditor.apply();
    }

    //видалити файли конфігурації
    public void removeURL() {
        mEditor.remove(URL);
        mEditor.apply();
    }

    //отримати дані авторизації
    public AccountEntity getAuthorizationData() {
        AccountEntity authorizationData = new AccountEntity();
        authorizationData.setEncryptedLogin(mStorage.getString(LOGIN, ""));
        authorizationData.setEncryptedPassword(mStorage.getString(PASSWORD, ""));
        return authorizationData;
    }

    public String getURL() {
        return mStorage.getString(URL, "http://192.168.0.104:8080/");
    }

    //перевірити наявність збережених авторизаційних даних
    public boolean containsAuthorizationData() {
        return mStorage.contains(LOGIN) && mStorage.contains(PASSWORD);
    }
}
