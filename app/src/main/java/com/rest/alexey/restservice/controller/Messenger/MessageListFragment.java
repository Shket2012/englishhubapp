package com.rest.alexey.restservice.controller.Messenger;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.ConversationEntity;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.entity.MessageEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.ConversationREST;
import com.rest.alexey.restservice.model.rest.impl.MessageREST;
import com.rest.alexey.restservice.model.task.AsyncREST;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.Date;
import java.util.List;

import static com.rest.alexey.restservice.util.Authorization.getAuthorization;

/**
 * Клас фрагменту для відображення списку повідомлень
 */

public class MessageListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private MessageItemAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    private RelativeLayout mSendBlockRelativeLayout;

    private TextView mEnterTextView;
    private ImageButton mSendImageButton;

    private long mIdConversation;

    //обробник створення фрагменту
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mIdConversation = getArguments().getLong("idConversation");
    }

    //обробник створення представлення фрагменту
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_messages, container, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_messages_recycler_view);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mSendBlockRelativeLayout = (RelativeLayout) view.findViewById(R.id.fragment_list_messages_send_block);

        mEnterTextView = (TextView) view.findViewById(R.id.fragment_list_messages_enter_text_edit_text);
        mSendImageButton = (ImageButton) view.findViewById(R.id.fragment_list_messages_send_button);
        mSendImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConversationEntity conversation = new ConversationEntity();
                conversation.setId(mIdConversation);

                UserEntity author = getAuthorization().getAuthorizationData().getUser();

                String receivedMessage = mEnterTextView.getText().toString().trim();
                if (receivedMessage.isEmpty()) {
                    return;
                }

                MessageEntity message = new MessageEntity(conversation, author, receivedMessage,
                        new Date());

                new SendMessageTask(message).execute();
            }
        });

        restrictSendingMessages();

        //отримання повідомлень та відображення їх
        new GetMessagesAndUpdateUI().execute();

        return view;
    }

    //перевірити та заборонити відправку повідомлень
    private void restrictSendingMessages() {
        AccountEntity authorizedAccount = Authorization.getAuthorization().getAuthorizationData();
        GroupEntity userGroup = authorizedAccount.getUser().getGroup();

        if (userGroup != null) {
            if (userGroup.getEndDate().compareTo(new Date()) < 0 && !authorizedAccount.isAdmin()) {
                mSendBlockRelativeLayout.setVisibility(View.INVISIBLE);
            }
        }
    }

    //створення меню
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_conversation, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //обробник натискання на пункт меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            //оновлення
            case R.id.menu_action_update:
                new GetMessagesAndUpdateUI().execute();
                return true;
            case R.id.menu_action_leave:
                new LeaveConversationTask(getContext()).execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //оновлення інтерфейсу
    private void updateUI(List<MessageEntity> items) {
        mAdapter = new MessageItemAdapter(items);
        mLinearLayoutManager.scrollToPosition(items.size()-1);
        mRecyclerView.setAdapter(mAdapter);
    }

    private class MessageItemAdapter extends RecyclerView.Adapter<MessageHolder> {
        private List<MessageEntity> mItems;

        public MessageItemAdapter(List<MessageEntity> items) {
            this.mItems = items;
        }

        @Override
        public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_message, parent, false);
            return new MessageHolder(view);
        }

        //зв'язування з представленням
        @Override
        public void onBindViewHolder(MessageHolder holder, int position) {
            MessageEntity item = mItems.get(position);
            holder.bind(item);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    private class GetMessagesAndUpdateUI extends AsyncTask<Void, Void, REST.Result<List<MessageEntity>>> {

        @Override
        protected REST.Result<List<MessageEntity>> doInBackground(Void... params) {
            MessageREST messageREST = new MessageREST();

            REST.Result<List<MessageEntity>> result;

            //отримання повідомлень
            try {
                List<MessageEntity> messages = messageREST.getByConversation(mIdConversation);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, messages);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        //оновлення інтерфейсу
        @Override
        protected void onPostExecute(REST.Result<List<MessageEntity>> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                updateUI(result.getResponse());
            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)) {
                Toast.makeText(MessageListFragment.this.getActivity(), R.string.access_denied,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(MessageListFragment.this.getActivity(), R.string.cannot_connect,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private class SendMessageTask extends AsyncTask<Void, Void, REST.Result<Long>> {

        private MessageEntity mEntity;
        private MessageREST mMessageREST = new MessageREST();

        public SendMessageTask(final MessageEntity entity) {
            this.mEntity = entity;
        }

        @Override
        protected REST.Result<Long> doInBackground(Void... params) {
            REST.Result<Long> result;

            try {
                Long idEntity = mMessageREST.add(mEntity);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, idEntity);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<Long> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                new GetMessagesAndUpdateUI().execute();
                mEnterTextView.setText("");
            } else {
                Toast.makeText(getActivity(), R.string.add_toast_unsuccessful,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private class LeaveConversationTask extends AsyncREST<Boolean> {

        LeaveConversationTask(Context context) {
            super(context);
        }

        @Override
        protected Boolean executeREST() {
            ConversationREST conversationREST = new ConversationREST();
            return conversationREST.leave(mIdConversation);
        }

        @Override
        protected void onSuccessfulExecute(Boolean result) {
            Toast.makeText(getActivity(), R.string.fragment_list_messages_leave_successful,
                    Toast.LENGTH_SHORT)
                    .show();
            GlobalFragmentManager.popBackStack(1, getActivity());
        }

        @Override
        protected void onUnsuccessfulExecute() {
            Toast.makeText(getActivity(), R.string.fragment_list_messages_leave_unsuccessful,
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }
}
