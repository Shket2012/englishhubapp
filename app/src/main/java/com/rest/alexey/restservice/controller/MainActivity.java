package com.rest.alexey.restservice.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AdminMenuItem.AdminMenuListFragment;
import com.rest.alexey.restservice.controller.Messenger.ConversationListFragment;
import com.rest.alexey.restservice.controller.News.NewsListFragment;
import com.rest.alexey.restservice.controller.Profile.ProfileFragment;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

/**
 * Клас активності для розміщення фрагметів
 */

public class MainActivity extends AppCompatActivity {

    //налаштування навігаційної панелі
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_news:
                    changeFragment(new NewsListFragment());
                    return true;
                case R.id.navigation_profile:
                    changeFragment(new ProfileFragment());
                    return true;
                case R.id.navigation_messenger:
                    changeFragment(new ConversationListFragment());
                    return true;
                case R.id.navigation_admin:
                    changeFragment(new AdminMenuListFragment());
                    return true;
            }
            return false;
        }

    };

    //отробник на створення активності
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Authorization authorization = Authorization.getAuthorization();

        int activity_id;

        if (authorization.getAuthorizationData().isAdmin()) {
            activity_id = R.layout.activity_main_admin;
        } else {
            activity_id = R.layout.activity_main_user;
        }

        setContentView(activity_id);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (GlobalFragmentManager.getCurrentFragment() == null) {
            GlobalFragmentManager.replace(new NewsListFragment(), this);
        } else {
            GlobalFragmentManager.replaceOnCurrent(this);
        }
    }

    //змінює фрагмент
    private void changeFragment(final Fragment fragment) {
        GlobalFragmentManager.clearBackStack(this);
        GlobalFragmentManager.replace(fragment, this);
    }

    //допомагає створити новий інтент
    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, MainActivity.class);
        return intent;
    }
}