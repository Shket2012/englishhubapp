package com.rest.alexey.restservice.controller.Group;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.rest.impl.GroupREST;
import com.rest.alexey.restservice.model.task.EditTask;
import com.rest.alexey.restservice.util.DateFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Клас фрагменту для редагування групи
 */

public class EditGroupEntityFragment extends AbstractFragment {

    private EditText mNameEditText;
    private EditText mLevelEditText;
    private EditText mStartDateEditText;
    private EditText mEndDateEditText;
    private EditText mScheduleEditText;
    private EditText mTrainingMaterialsEditText;

    private Button mEditButton;

    private GroupEntity mGroupEntity;

    //обробник створення екземпляру
    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);

        mGroupEntity = (GroupEntity) getArguments()
                .getSerializable(GroupEntity.class.getName());
    }

    //пошук віджетів
    @Override
    protected void findWidgets(View view) {
        mNameEditText = (EditText) view.findViewById(R.id.edit_group_name_edit_text);
        mLevelEditText = (EditText) view.findViewById(R.id.edit_group_level_edit_text);
        mStartDateEditText = (EditText) view.findViewById(R.id.edit_group_start_date_edit_text);
        mEndDateEditText = (EditText) view.findViewById(R.id.edit_group_end_date_edit_text);
        mScheduleEditText = (EditText) view.findViewById(R.id.edit_group_schedule_edit_text);
        mTrainingMaterialsEditText = (EditText) view.findViewById(R.id.edit_group_training_materials_edit_text);

        mEditButton = (Button) view.findViewById(R.id.edit_group_button);
    }

    //налаштування віджетів
    @Override
    protected void setUpWidgets() {

        mNameEditText.setText(mGroupEntity.getName());
        mLevelEditText.setText(String.valueOf(mGroupEntity.getLevel()));
        mStartDateEditText.setText(DateFormatter.formatByDayMonthYear(mGroupEntity.getStartDate()));
        mEndDateEditText.setText(DateFormatter.formatByDayMonthYear(mGroupEntity.getEndDate()));
        mScheduleEditText.setText(mGroupEntity.getSchedule());

        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String receivedName = mNameEditText.getText().toString().trim();

                if (receivedName.equals("")) {
                    Toast.makeText(getContext(), R.string.incorrect_data, Toast.LENGTH_SHORT).show();
                    return;
                }
                mGroupEntity.setName(receivedName);

                DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                Date startDate;
                Date endDate;

                final int parsedLevel;

                try {
                    startDate = format.parse(mStartDateEditText.getText().toString());
                    endDate = format.parse(mEndDateEditText.getText().toString());

                    parsedLevel = Integer.valueOf(mLevelEditText.getText().toString());

                } catch (ParseException e) {
                    Toast.makeText(getContext(), R.string.incorrect_date, Toast.LENGTH_SHORT).show();
                    return;
                } catch (NumberFormatException exc) {
                    Toast.makeText(getContext(), R.string.incorrect_data, Toast.LENGTH_SHORT).show();
                    return;
                }

                mGroupEntity.setLevel(parsedLevel);

                mGroupEntity.setStartDate(startDate);
                mGroupEntity.setEndDate(endDate);

                mGroupEntity.setSchedule(mScheduleEditText.getText().toString().trim());
                mGroupEntity.setTrainingMaterials(mTrainingMaterialsEditText.getText().toString().trim());

                new EditTask<>(new GroupREST(), mGroupEntity, getContext()).execute();
            }
        });
    }

    //встановлення макету
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_group;
    }
}
