package com.rest.alexey.restservice.controller.Messenger;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.User.AddUserEntityFragment;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.UserREST;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.List;

/**
 * Клас фрагменту для додавання розмови
 */

public class AddConversationFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private UserItemAdapter mAdapter;

    //обробник створення фрагменту
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //need to get data
        new GetUsersAndUpdateUI().execute();

        return view;
    }

    //обробник створення меню
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(Authorization.getAuthorization().getAuthorizationData().isAdmin()) {
            inflater.inflate(R.menu.menu_list, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    //обробник натискання на пункт меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_action_add:
                GlobalFragmentManager.replaceAndAddToBackStack(new AddUserEntityFragment(), getActivity());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //оновлення профілю
    private void updateUI(List<UserEntity> items) {
        mAdapter = new UserItemAdapter(items);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Клас для адаптування користувача до представлення
     */

    private class UserItemAdapter extends RecyclerView.Adapter<UserForConversationHolder> {
        private List<UserEntity> mItems;

        public UserItemAdapter(List<UserEntity> items) {
            this.mItems = items;
        }

        @Override
        public UserForConversationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_user_for_conversation, parent, false);
            return new UserForConversationHolder(view);
        }

        //обробник зв'язування
        @Override
        public void onBindViewHolder(UserForConversationHolder holder, int position) {
            UserEntity item = mItems.get(position);
            holder.bind(item);
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    /**
     * Клас для отримання користувачів та оновлення інтерфейсу
     */

    private class GetUsersAndUpdateUI extends AsyncTask<Void, Void, REST.Result<List<UserEntity>>> {

        @Override
        protected REST.Result<List<UserEntity>> doInBackground(Void... params) {
            UserREST userREST = new UserREST();

            REST.Result<List<UserEntity>> result;

            AccountEntity authorizedUser = Authorization.getAuthorization().getAuthorizationData();

            //отримання користувачів
            try {

                List<UserEntity> users;

                if (authorizedUser.isAdmin()) {
                    users = userREST.getAll();
                } else {
                    GroupEntity group = authorizedUser.getUser().getGroup();
                    users = userREST.getAllUserByGroup(group.getId());
                }

                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, users);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        //викликається після виконання doInBackground()
        @Override
        protected void onPostExecute(REST.Result<List<UserEntity>> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                updateUI(result.getResponse());
            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)){
                Toast.makeText(AddConversationFragment.this.getActivity(), R.string.access_denied,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(AddConversationFragment.this.getActivity(), R.string.cannot_connect,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
