package com.rest.alexey.restservice.model.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.rest.REST;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

public abstract class AsyncREST<ResultType> extends AsyncTask<Void, Void, REST.Result<ResultType>> {

    private Context mContext;

    public AsyncREST(Context context) {
        this.mContext = context;
    }

    @Override
    protected REST.Result<ResultType> doInBackground(Void... params) {
        REST.Result<ResultType> resultREST;

        try {
            ResultType result = executeREST();
            resultREST = new REST.Result<>(REST.AccessResult.SUCCESSFUL, result);

        } catch (RestClientException exc) {
            Log.w("AsyncREST", exc.getMessage());

            if (exc instanceof ResourceAccessException) {
                resultREST = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
            } else {
                resultREST = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
            }
        }
        return resultREST;
    }

    protected abstract ResultType executeREST();

    protected abstract void onSuccessfulExecute(ResultType result);

    protected abstract void onUnsuccessfulExecute();

    protected void onCannotConnect() {
        Toast.makeText(mContext, R.string.cannot_connect,
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected void onPostExecute(REST.Result<ResultType> executeResult) {
        REST.AccessResult accessResult = executeResult.getAccessResult();
        ResultType result = executeResult.getResponse();

        if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
            onSuccessfulExecute(result);
        } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)) {
            onUnsuccessfulExecute();
        } else {
            onCannotConnect();
        }
    }
}