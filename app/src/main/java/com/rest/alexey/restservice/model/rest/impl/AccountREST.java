package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.util.Authorization;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class AccountREST extends REST<AccountEntity> {
    public AccountREST() {
        super("accounts/");
    }

    public long add(AccountEntity entity) {
        HttpEntity<AccountEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Long> response = sRestTemplate.exchange(sURL, HttpMethod.POST, request,
                new ParameterizedTypeReference<Long>() {});

        return response.getBody();
    }

    public AccountEntity authorize(AccountEntity authDate) {
        HttpEntity request = new HttpEntity(Authorization.getHttpHeadersWithAuthData(authDate));

        ResponseEntity<AccountEntity> response = sRestTemplate.exchange(sURL + "authorize/", HttpMethod.GET, request,
                new ParameterizedTypeReference<AccountEntity>() {
                });
        return response.getBody();
    }

    public AccountEntity getCurrent() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<AccountEntity> response = sRestTemplate.exchange(sURL + "current/", HttpMethod.GET, request,
                new ParameterizedTypeReference<AccountEntity>() {
                });
        return response.getBody();
    }

    public boolean delete(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public AccountEntity get(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<AccountEntity> response = sRestTemplate.exchange(sURL + id, HttpMethod.GET, request,
                new ParameterizedTypeReference<AccountEntity>() {
                });
        return response.getBody();
    }

    public List<AccountEntity> getAll() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<AccountEntity>> response = sRestTemplate.exchange(sURL, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<AccountEntity>>() {
                });
        return response.getBody();
    }

    public boolean update(long id, AccountEntity entity) {
        HttpEntity<AccountEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public boolean updateCurrent(AccountEntity entity) {
        HttpEntity<AccountEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + "current/", HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }
}
