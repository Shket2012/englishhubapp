package com.rest.alexey.restservice.util;

import com.rest.alexey.restservice.model.entity.AccountEntity;

import org.springframework.http.HttpHeaders;

/**
 * Клас-сингилтон для роботи з авторизаційними даними
 */

public class Authorization {

    private static Authorization sAuthorization;

    private AccountEntity mAuthorizationData;

    private Authorization() {
        mAuthorizationData = new AccountEntity();
    }

    public static Authorization getAuthorization() {
        if (sAuthorization == null) {
            sAuthorization = new Authorization();
        }
        return sAuthorization;
    }

    //отримати авторизаційні дані
    public AccountEntity getAuthorizationData() {
        return mAuthorizationData;
    }

    //встановити авторизаційні дані
    public void setAuthorizationData(AccountEntity authorizationData) {
        this.mAuthorizationData = authorizationData;
    }

    //вкласти авторизаційні дані у HttpHeaders
    private static HttpHeaders putIntoHttpHeadersAuthData(final AccountEntity authData, final HttpHeaders headers) {
        headers.add("login", authData.getEncryptedLogin());
        headers.add("password", authData.getEncryptedPassword());
        return headers;
    }

    //отримати HttpHeaders з авторизаційними даними
    public HttpHeaders getHttpHeadersWithAuthData() {
        return putIntoHttpHeadersAuthData(mAuthorizationData,new HttpHeaders());
    }

    //отримати HttpHeaders з авторизаційними даними
    public static HttpHeaders getHttpHeadersWithAuthData(final AccountEntity authData) {
        return putIntoHttpHeadersAuthData(authData, new HttpHeaders());
    }
}
