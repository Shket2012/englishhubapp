package com.rest.alexey.restservice.controller.User;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.model.rest.impl.GroupREST;
import com.rest.alexey.restservice.model.task.EditTask;
import com.rest.alexey.restservice.util.DateFormatter;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditUserEntityFragment extends AbstractFragment {

    private EditText mLastNameEditText;
    private EditText mFirstNameEditText;
    private EditText mMiddleNameEditText;
    private EditText mDateBirthEditText;

    private EditText mLoginEditText;
    private EditText mPasswordEditText;

    private Spinner mGroupSpinner;
    private CheckBox mAdminCheckBox;

    private Button mEditButton;

    private AccountEntity mAccountEntity;
    private List<GroupEntity> mGroups;

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);

        mAccountEntity = (AccountEntity) getArguments()
                .getSerializable(AccountEntity.class.getName());
    }

    @Override
    protected void findWidgets(View view) {
        mLastNameEditText = (EditText) view.findViewById(R.id.edit_user_last_name_edit_text);
        mFirstNameEditText = (EditText) view.findViewById(R.id.edit_user_first_name_edit_text);
        mMiddleNameEditText = (EditText) view.findViewById(R.id.edit_user_middle_name_edit_text);
        mDateBirthEditText = (EditText) view.findViewById(R.id.edit_user_birth_date_edit_text);
        mLoginEditText = (EditText) view.findViewById(R.id.edit_user_login_edit_text);
        mPasswordEditText = (EditText) view.findViewById(R.id.edit_user_password_edit_text);

        mGroupSpinner = (Spinner) view.findViewById(R.id.edit_user_group_spinner);
        mAdminCheckBox = (CheckBox) view.findViewById(R.id.edit_user_admin_check_box);

        mEditButton = (Button) view.findViewById(R.id.edit_user_button);
    }

    @Override
    protected void setUpWidgets() {

        new GetGroupsAndSetUpGroupSpinner().execute();

        mLastNameEditText.setText(mAccountEntity.getUser().getLastName());
        mFirstNameEditText.setText(mAccountEntity.getUser().getFirstName());
        mMiddleNameEditText.setText(mAccountEntity.getUser().getMiddleName());
        mDateBirthEditText.setText(DateFormatter.formatByDayMonthYear(mAccountEntity.getUser().getBirthDate()));

        mAdminCheckBox.setChecked(mAccountEntity.isAdmin());

        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAccountEntity.getUser().setLastName(mLastNameEditText.getText().toString());
                mAccountEntity.getUser().setFirstName(mFirstNameEditText.getText().toString());
                mAccountEntity.getUser().setMiddleName(mMiddleNameEditText.getText().toString());

                DateFormat format = new SimpleDateFormat("ddd.mm.yyyy", Locale.ENGLISH);
                Date dateBirth = null;
                try {
                    dateBirth = format.parse(mDateBirthEditText.getText().toString());
                } catch (ParseException e) {
                    Toast.makeText(getActivity(), "Incorrect date !", Toast.LENGTH_SHORT).show();
                }

                if (dateBirth == null) {
                    return;
                }

                mAccountEntity.getUser().setBirthDate(dateBirth);

                int selectedPositionGroup = mGroupSpinner.getSelectedItemPosition();
                GroupEntity selectedGroup = mGroups.get(selectedPositionGroup);

                mAccountEntity.getUser().setGroup(selectedGroup);

                if (!mLoginEditText.getText().toString().trim().equals("")) {
                    mAccountEntity.setLoginAndEncrypt(mLoginEditText.getText().toString());
                }
                if (!mPasswordEditText.getText().toString().trim().equals("")) {
                    mAccountEntity.setPasswordAndEncrypt(mPasswordEditText.getText().toString());
                }

                mAccountEntity.setAdmin(mAdminCheckBox.isChecked());

                new EditTask<AccountEntity, AccountREST>(new AccountREST(), mAccountEntity, getContext()).execute();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_user;
    }

    private void setUpGroupSpinner(final List<CharSequence> groups) {
        ArrayAdapter<CharSequence> groupAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, groups);

        mGroupSpinner.setAdapter(groupAdapter);
    }

    private class GetGroupsAndSetUpGroupSpinner extends AsyncTask<Void, Void, REST.Result<List<GroupEntity>>> {

        private GroupREST groupREST = new GroupREST();

        @Override
        protected REST.Result<List<GroupEntity>>doInBackground(Void... params) {
            REST.Result<List<GroupEntity>> result;

            try {
                List<GroupEntity> groups = groupREST.getAll();
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, groups);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<List<GroupEntity>> result) {
            REST.AccessResult accessResult = result.getAccessResult();
            mGroups = result.getResponse();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL) && mGroups.size() > 0) {
                List<CharSequence> groupNames = new ArrayList<>();

                for (GroupEntity group : mGroups) {
                    groupNames.add(group.getName());
                }

                setUpGroupSpinner(groupNames);
            } else {
                mEditButton.setEnabled(false);
            }
        }
    }
}
