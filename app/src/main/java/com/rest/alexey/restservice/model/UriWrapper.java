package com.rest.alexey.restservice.model;

import android.net.Uri;

import java.io.Serializable;

/**
 * Клас-обгортка для реалізації можливості
 * серіалізації Uri
 */

public class UriWrapper implements Serializable {
    public Uri getUri() {
        return mUri;
    }

    public void setmUri(Uri mUri) {
        this.mUri = mUri;
    }

    private Uri mUri;

    public UriWrapper(Uri uri) {
        this.mUri = uri;
    }
}
