package com.rest.alexey.restservice.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Клас для роботи з файлами
 */

public class FileUtils {

    //зчитування файлу
    public static byte[] readFile(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();
        }
    }

    //отримання повного шляху
    public static String getRealPathFromURI(Uri contentUri, Context context) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    //запис файлу
    public static Uri writeFile(byte[] file, String name) throws IOException {
        File externalPath = Environment.getExternalStorageDirectory();

        externalPath = new File(externalPath.getPath() + "/Android/data/com.alexey.englishhub/images/");

        if (!externalPath.exists()) {
            externalPath.mkdirs();
        }

        String imagePath = externalPath.getPath() + "/" + name;
        FileOutputStream fos = new FileOutputStream(imagePath);

        fos.write(file);
        fos.close();

        return Uri.fromFile(new File(imagePath));
    }
}
