package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.entity.ConversationEntity;
import com.rest.alexey.restservice.model.rest.REST;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ConversationREST extends REST<ConversationEntity> {
    public ConversationREST() {
        super("conversations/");
    }

    public long add(ConversationEntity entity) {
        HttpEntity<ConversationEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Long> response = sRestTemplate.exchange(sURL, HttpMethod.POST, request,
                new ParameterizedTypeReference<Long>() {});

        return response.getBody();
    }

    public boolean delete(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public boolean leave(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + "leave/" + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public ConversationEntity get(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<ConversationEntity> response = sRestTemplate.exchange(sURL + id, HttpMethod.GET, request,
                new ParameterizedTypeReference<ConversationEntity>() {
                });
        return response.getBody();
    }

    public List<ConversationEntity> getAll() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<ConversationEntity>> response = sRestTemplate.exchange(sURL, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<ConversationEntity>>() {
                });
        return response.getBody();
    }

    public List<ConversationEntity> getAllPersonal() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<ConversationEntity>> response = sRestTemplate.exchange(sURL + "personal/", HttpMethod.GET, request,
                new ParameterizedTypeReference<List<ConversationEntity>>() {
                });
        return response.getBody();
    }

    public boolean update(long id, ConversationEntity entity) {
        HttpEntity<ConversationEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }
}
