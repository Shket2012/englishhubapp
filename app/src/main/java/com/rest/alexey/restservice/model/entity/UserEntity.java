package com.rest.alexey.restservice.model.entity;

import java.util.Date;

/**
 * Клас сутності користувач
 */

public class UserEntity extends AbstractEntity {

    private String lastName;
    private String firstName;
    private String middleName;
    private Date birthDate;

    private String imageName;

    private GroupEntity group;

    public UserEntity() {}
    public UserEntity(final String lastName, final String firstName, final String middleName,
                      final Date birthDate, String imageName, final GroupEntity group) {
        this.lastName = lastName;
        this.middleName = middleName;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.imageName = imageName;
        this.group = group;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity group) {
        this.group = group;
    }
}