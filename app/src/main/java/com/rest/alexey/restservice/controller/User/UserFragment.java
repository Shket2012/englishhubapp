package com.rest.alexey.restservice.controller.User;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.model.task.DeleteTask;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

public class UserFragment extends AbstractFragment {

    private TextView mLastNameTextView;
    private TextView mFirstNameTextView;
    private TextView mMiddleNameTextView;
    private TextView mDateBirthTextView;

    private TextView mGroupNameTextView;
    private TextView mGroupLevelTextView;
    private TextView mStartDateTextView;
    private TextView mEndDateTextView;
    private TextView mScheduleTextView;

    private AccountEntity mAccountEntity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccountEntity = (AccountEntity) getArguments()
                .getSerializable(AccountEntity.class.getName());
    }

    @Override
    protected void findWidgets(View view) {
        mLastNameTextView = (TextView)  view.findViewById(R.id.profile_last_name_text_view);
        mFirstNameTextView = (TextView) view.findViewById(R.id.profile_first_name_text_view);
        mMiddleNameTextView = (TextView) view.findViewById(R.id.profile_middle_name_text_view);
        mDateBirthTextView = (TextView) view.findViewById(R.id.profile_birthday_text_view);

        mGroupNameTextView = (TextView) view.findViewById(R.id.profile_group_name_text_view);
        mGroupLevelTextView = (TextView) view.findViewById(R.id.profile_group_level_text_view);
        mStartDateTextView = (TextView) view.findViewById(R.id.profile_group_start_date_text_view);
        mEndDateTextView = (TextView) view.findViewById(R.id.profile_group_end_date_text_view);
        mScheduleTextView = (TextView) view.findViewById(R.id.profile_group_schedule_text_view);

    }

    @Override
    protected void setUpWidgets() {
        setHasOptionsMenu(true);

        UserEntity userEntity = mAccountEntity.getUser();

        mLastNameTextView.setText(userEntity.getLastName());
        mFirstNameTextView.setText(userEntity.getFirstName());
        mMiddleNameTextView.setText(userEntity.getMiddleName());
        mDateBirthTextView.setText(DateFormatter.formatByDayMonthYear(userEntity.getBirthDate()));

        GroupEntity group = userEntity.getGroup();

        if (group != null) {
            mGroupNameTextView.setText(group.getName());
            mGroupLevelTextView.setText(String.valueOf(group.getLevel()));
            mStartDateTextView.setText(DateFormatter.formatByDayMonthYear(group.getStartDate()));
            mEndDateTextView.setText(DateFormatter.formatByDayMonthYear(group.getEndDate()));
            mScheduleTextView.setText(group.getSchedule());
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_full_information, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_action_edit:
                Bundle bundle = new Bundle();
                bundle.putSerializable(mAccountEntity.getClass().getName(), mAccountEntity);

                GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new EditUserEntityFragment(),
                        bundle, getActivity());
                return true;
            case R.id.menu_action_delete:
                new DeleteTask<AccountEntity, AccountREST>(new AccountREST(), mAccountEntity, getContext()).execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
