package com.rest.alexey.restservice.model;

import android.support.v4.app.Fragment;

public class AdminMenuItem {

    private String title;

    private Fragment fragment;

    public AdminMenuItem() {}

    public AdminMenuItem(final String title, final Fragment fragment) {
        this.title = title;
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(final Fragment fragment) {
        this.fragment = fragment;
    }
}
