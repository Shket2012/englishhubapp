package com.rest.alexey.restservice.controller;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.util.AppConfig;
import com.rest.alexey.restservice.util.AppServerURL;
import com.rest.alexey.restservice.util.Authorization;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

/**
 * Клас активності для авторизації
 */

public class AuthorizationActivity extends AppCompatActivity {

    private TextView mObservationTextView;

    private EditText mLoginEditText;
    private EditText mPasswordEditText;

    private Button mLogInButton;

    private AppConfig mConfig;

    private AccountEntity mAuthorizationData = new AccountEntity();

    //обробник на створення активності
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        mObservationTextView = (TextView) findViewById(R.id.login_observation);

        mLoginEditText = (EditText) findViewById(R.id.login_edit_text);
        mPasswordEditText = (EditText) findViewById(R.id.password_edit_text);

        mLogInButton = (Button) findViewById(R.id.log_in_button);
        mLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check login and password
                mAuthorizationData.setLoginAndEncrypt(mLoginEditText.getText().toString());
                mAuthorizationData.setPasswordAndEncrypt(mPasswordEditText.getText().toString());

                new CheckAuthorizationData().execute();
            }
        });

        //gets settings
        mConfig = new AppConfig(this);

        AppServerURL.get().setURL(mConfig.getURL());

        //Checks authorization data
        if (mConfig.containsAuthorizationData()) {
            mAuthorizationData = mConfig.getAuthorizationData();

            //Checks Authorization Data and Starts MainActivity
            // if Authorization Data is correct
            new CheckAuthorizationData().execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_authorization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_action_settings:
                startActivity(SettingsActivity.newIntent(this));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Intent newIntent(final Context packageContext) {
        return new Intent(packageContext, AuthorizationActivity.class);
    }

    //встановлює активними віджети для логіну
    public void setEnabledLoginWidgets(final boolean bool) {
        mLoginEditText.setEnabled(bool);
        mPasswordEditText.setEnabled(bool);
        mLogInButton.setEnabled(bool);
    }

    //відкриває активність MainActivity
    private void showMainActivity() {
        Intent intent = MainActivity.newIntent(this);
        startActivity(intent);
        //Finishes the current Activity
        finish();
    }

    //показує повідомлення користувачу
    private void showObservation(int color, int textId, boolean enableLoginWidgets) {
        mObservationTextView.setTextColor(color);
        mObservationTextView.setText(textId);
        setEnabledLoginWidgets(enableLoginWidgets);
    }

    /**
     * Клас для перевірки авторизаційних даних
     */

    private class CheckAuthorizationData extends AsyncTask<Void, Void, REST.Result<AccountEntity>> {

        private AccountREST accountREST = new AccountREST();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showObservation(getResources().getColor(R.color.colorPrimary),
                    R.string.login_observation_checking_data, false);
        }

        @Override
        protected REST.Result<AccountEntity> doInBackground(Void... params) {
            REST.Result<AccountEntity> result;

            //авторизація
            try {
                AccountEntity account = accountREST.authorize(mAuthorizationData);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, account);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<AccountEntity> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                AccountEntity confirmedAccount = result.getResponse();

                Authorization authorization = Authorization.getAuthorization();
                authorization.setAuthorizationData(confirmedAccount);

                mConfig.storeAuthorizationData(confirmedAccount);
                showMainActivity();

            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)){
                showObservation(getResources().getColor(R.color.holo_red_light),
                        R.string.login_observation, true);

                mPasswordEditText.setText("");
            } else {
                showObservation(getResources().getColor(R.color.holo_red_light),
                        R.string.cannot_connect, true);
            }
        }
    }
}
