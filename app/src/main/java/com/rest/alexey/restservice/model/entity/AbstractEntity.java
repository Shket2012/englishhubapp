package com.rest.alexey.restservice.model.entity;

import java.io.Serializable;

/**
 * Клас абстрактної сутності
 */

public class AbstractEntity implements Serializable {

    private long id;

    public AbstractEntity() {}

    public void setId(final long id) {this.id = id; }

    public long getId() {
        return id;
    }
}
