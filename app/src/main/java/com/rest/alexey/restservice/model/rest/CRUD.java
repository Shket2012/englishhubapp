package com.rest.alexey.restservice.model.rest;

import java.util.List;

/**
 * Інтерфейс за патерном CRUD
 */

public interface CRUD<T> {
    public abstract long add(T entity);

    public abstract boolean delete(long id);

    public abstract T get(long id);

    public abstract List<T> getAll();

    public abstract boolean update(long id, T entity);
}
