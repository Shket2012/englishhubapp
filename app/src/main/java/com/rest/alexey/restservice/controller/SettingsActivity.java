package com.rest.alexey.restservice.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.util.AppConfig;
import com.rest.alexey.restservice.util.AppServerURL;

public class SettingsActivity extends AppCompatActivity {

    private EditText mURLEditText;
    private Button mApplyButton;

    private AppConfig mConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mConfig = new AppConfig(this);

        mURLEditText = (EditText) findViewById(R.id.activity_settings_url_edit_text);
        mApplyButton = (Button) findViewById(R.id.activity_settings_apply_button);
        mApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mURLEditText.getText().toString().trim();
                mConfig.storeURL(url);
                AppServerURL.get().setURL("http://" + url + ":8080/");

                Toast.makeText(getBaseContext(), R.string.successful, Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    public static Intent newIntent(final Context packageContext) {
        return new Intent(packageContext, SettingsActivity.class);
    }

}
