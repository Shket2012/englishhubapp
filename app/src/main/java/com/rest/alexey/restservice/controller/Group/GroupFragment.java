package com.rest.alexey.restservice.controller.Group;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.rest.impl.GroupREST;
import com.rest.alexey.restservice.model.task.DeleteTask;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

/**
 * Клас фрагменту для відображення групи
 */

public class GroupFragment extends AbstractFragment {
    
    private TextView mGroupNameTextView;
    private TextView mGroupLevelTextView;
    private TextView mStartDateTextView;
    private TextView mEndDateTextView;
    private TextView mScheduleTextView;
    private TextView mTrainingMaterialsTextView;

    private GroupEntity mGroupEntity;

    //обробник створення екземпляру
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGroupEntity = (GroupEntity) getArguments()
                .getSerializable(GroupEntity.class.getName());
    }

    //пошук віджетів
    @Override
    protected void findWidgets(View view) {
        mGroupNameTextView = (TextView) view.findViewById(R.id.group_name_text_view);
        mGroupLevelTextView = (TextView) view.findViewById(R.id.group_level_text_view);
        mStartDateTextView = (TextView) view.findViewById(R.id.group_start_date_text_view);
        mEndDateTextView = (TextView) view.findViewById(R.id.group_end_date_text_view);
        mScheduleTextView = (TextView) view.findViewById(R.id.group_schedule_text_view);
        mTrainingMaterialsTextView = (TextView) view.findViewById(R.id.group_training_materials_text_view);
    }

    //налаштування віджетів
    @Override
    protected void setUpWidgets() {
        setHasOptionsMenu(true);

        mGroupNameTextView.setText(mGroupEntity.getName());
        mGroupLevelTextView.setText(String.valueOf(mGroupEntity.getLevel()));
        mStartDateTextView.setText(DateFormatter.formatByDayMonthYear(mGroupEntity.getStartDate()));
        mEndDateTextView.setText(DateFormatter.formatByDayMonthYear(mGroupEntity.getEndDate()));
        mScheduleTextView.setText(mGroupEntity.getSchedule());
        mTrainingMaterialsTextView.setText(mGroupEntity.getTrainingMaterials());
    }

    //встановлення макету
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_group;
    }

    //обробник створення меню
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_full_information, menu);
    }

    //обробник вибору пункту меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            //редагування
            case R.id.menu_action_edit:
                Bundle bundle = new Bundle();
                bundle.putSerializable(mGroupEntity.getClass().getName(), mGroupEntity);

                GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new EditGroupEntityFragment(),
                        bundle, getActivity());
                return true;

            //видалення
            case R.id.menu_action_delete:
                new DeleteTask<GroupEntity, GroupREST>(new GroupREST(), mGroupEntity, getContext()).execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
