package com.rest.alexey.restservice.controller.Group;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

/**
 * Клас для утримання групи
 */

public class GroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private GroupEntity mGroup;

    private TextView mNameTextView;
    private TextView mLevelTextView;
    private TextView mStartDateTextView;
    private TextView mEndDateTextView;

    public GroupHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mNameTextView = (TextView) itemView.findViewById(R.id.list_item_group_name_text_view);
        mLevelTextView = (TextView) itemView.findViewById(R.id.list_item_group_level_text_view);
        mStartDateTextView = (TextView) itemView.findViewById(R.id.list_item_group_start_date_text_view);
        mEndDateTextView = (TextView) itemView.findViewById(R.id.list_item_group_end_date_text_view);
    }

    //налаштування макету
    public void bind(final GroupEntity item) {
        mGroup = item;
        mNameTextView.setText(item.getName());
        mLevelTextView.setText(String.valueOf(item.getLevel()));
        mStartDateTextView.setText(DateFormatter.formatByDayMonthYear(item.getStartDate()));
        mEndDateTextView.setText(DateFormatter.formatByDayMonthYear(item.getEndDate()));
    }

    //обробник натискання на групу
    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(mGroup.getClass().getName(), mGroup);

        GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new GroupFragment(),
                bundle, (MainActivity) v.getContext());
    }
}
