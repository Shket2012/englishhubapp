package com.rest.alexey.restservice.controller.Messenger;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.entity.MessageEntity;
import com.rest.alexey.restservice.model.entity.UserEntity;
import com.rest.alexey.restservice.util.DateFormatter;

/**
 * Клас для утримання повідомлень
 */

public class MessageHolder extends RecyclerView.ViewHolder {
    private MessageEntity mMessage;

    private TextView mDateTextView;
    private TextView mMessageTextView;
    private TextView mAuthorTextView;

    public MessageHolder(View itemView) {
        super(itemView);

        mDateTextView = (TextView) itemView.findViewById(R.id.list_item_message_date_text_view);
        mMessageTextView = (TextView) itemView.findViewById(R.id.list_item_message_text_text_view);
        mAuthorTextView = (TextView) itemView.findViewById(R.id.list_item_message_author_text_view);
    }

    //налаштування представлення
    public void bind(final MessageEntity item) {
        mMessage = item;

        UserEntity author = mMessage.getAuthor();

        mDateTextView.setText(DateFormatter.formatByDateTime(mMessage.getReleaseDate()));
        mMessageTextView.setText(mMessage.getMessage());
        mAuthorTextView.setText(author.getLastName() + " " + author.getFirstName());
    }
}
