package com.rest.alexey.restservice.model.entity;

import java.util.Date;

/**
 * Клас сутності новина
 */

public class NewsEntity extends AbstractEntity {

    private String title;
    private String content;
    private Date releaseDate;
    private String imageName;

    public NewsEntity() { }

    public NewsEntity(String title, String content, Date releaseDate) {
        this.title = title;
        this.content = content;
        this.releaseDate = releaseDate;
    }

    public NewsEntity(String title, String content, Date releaseDate, String imageName) {
        this.title = title;
        this.content = content;
        this.releaseDate = releaseDate;
        this.imageName = imageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }


    @Override
    public String toString() {
        return "Content: " + content + "\n" +
                "ReleaseDate: " + releaseDate + "\n";
    }
}