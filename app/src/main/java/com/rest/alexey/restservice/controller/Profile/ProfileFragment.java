package com.rest.alexey.restservice.controller.Profile;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AuthorizationActivity;
import com.rest.alexey.restservice.model.UriWrapper;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.FileDTO;
import com.rest.alexey.restservice.model.entity.GroupEntity;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.model.rest.impl.FileREST;
import com.rest.alexey.restservice.util.AppConfig;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.FileUtils;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import java.io.IOException;

public class ProfileFragment extends Fragment {

    private ImageView mImageView;

    private TextView mLastNameTextView;
    private TextView mFirstNameTextView;
    private TextView mMiddleNameTextView;
    private TextView mDateBirthTextView;

    private TextView mGroupNameTextView;
    private TextView mGroupLevelTextView;
    private TextView mStartDateTextView;
    private TextView mEndDateTextView;
    private TextView mScheduleTextView;
    private TextView mTrainingMaterialsTextView;

    private Uri mImageUri;

    private AppConfig mConfig;
    private AccountEntity mAccountEntity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mConfig = new AppConfig(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);

        mImageView = (ImageView) view.findViewById(R.id.profile_image_view);

        mLastNameTextView = (TextView) view.findViewById(R.id.profile_last_name_text_view);
        mFirstNameTextView = (TextView) view.findViewById(R.id.profile_first_name_text_view);
        mMiddleNameTextView = (TextView) view.findViewById(R.id.profile_middle_name_text_view);
        mDateBirthTextView = (TextView) view.findViewById(R.id.profile_birthday_text_view);

        mGroupNameTextView = (TextView) view.findViewById(R.id.profile_group_name_text_view);
        mGroupLevelTextView = (TextView) view.findViewById(R.id.profile_group_level_text_view);
        mStartDateTextView = (TextView) view.findViewById(R.id.profile_group_start_date_text_view);
        mEndDateTextView = (TextView) view.findViewById(R.id.profile_group_end_date_text_view);
        mScheduleTextView = (TextView) view.findViewById(R.id.profile_group_schedule_text_view);
        mTrainingMaterialsTextView = (TextView) view.findViewById(R.id.profile_group_training_materials_text_view);

        new HttpRequestGetAccount().execute();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_current_user, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_profile_action_log_out:
                mConfig.removeAuthorizationData();
                startActivity(AuthorizationActivity.newIntent(getActivity()));
                GlobalFragmentManager.setCurrentFragment(null);
                getActivity().finish();
                return true;

            case R.id.menu_profile_action_edit:
                Bundle bundle = new Bundle();
                bundle.putSerializable(AccountEntity.class.getName(), mAccountEntity);
                bundle.putSerializable("ProfileImage", new UriWrapper(mImageUri));

                GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new EditProfileFragment(),
                        bundle, getActivity());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class HttpRequestGetAccount extends AsyncTask<Void, Void, AccountEntity> {
        private byte[] mFile;
        private AccountEntity mAccount;

        @Override
        protected AccountEntity doInBackground(Void... params) {
            AccountREST accountREST = new AccountREST();
            FileREST fileREST = new FileREST();

            mAccount = accountREST.getCurrent();

            if (mAccount.getUser().getImageName() != null) {
                FileDTO fileDTO = fileREST.download(mAccount.getUser().getImageName());

                if (fileDTO != null) {
                    mFile = fileDTO.getFile();
                }
            }

            return mAccount;
        }

        @Override
        protected void onPostExecute(AccountEntity account) {
            mAccountEntity = account;

            mLastNameTextView.setText(account.getUser().getLastName());
            mFirstNameTextView.setText(account.getUser().getFirstName());
            mMiddleNameTextView.setText(account.getUser().getMiddleName());
            mDateBirthTextView.setText(DateFormatter.formatByDayMonthYear(account.getUser().getBirthDate()));

            if (mFile != null) {

                try {
                    mImageUri = FileUtils.writeFile(mFile, mAccount.getUser().getImageName());
                } catch (IOException e) {
                    Log.w(this.getClass().getName(), "Cannot write file: " + mAccount.getUser().getImageName());
                }

                if (mImageUri != null) {
                    mImageView.setImageURI(mImageUri);
                }
            }

            GroupEntity group = account.getUser().getGroup();

            if (group != null) {
                mGroupNameTextView.setText(group.getName());
                mGroupLevelTextView.setText(String.valueOf(group.getLevel()));
                mStartDateTextView.setText(DateFormatter.formatByDayMonthYear(group.getStartDate()));
                mEndDateTextView.setText(DateFormatter.formatByDayMonthYear(group.getEndDate()));
                mScheduleTextView.setText(group.getSchedule());
                mTrainingMaterialsTextView.setText(group.getTrainingMaterials());
            }
        }
    }
}
