package com.rest.alexey.restservice.controller.User;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.util.List;

public class UserListFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private UserItemAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //need to get data
        new GetUsersAndUpdateUI().execute();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(Authorization.getAuthorization().getAuthorizationData().isAdmin()) {
            inflater.inflate(R.menu.menu_list, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_action_add:
                GlobalFragmentManager.replaceAndAddToBackStack(new AddUserEntityFragment(), getActivity());
                return true;
            case R.id.menu_action_update:
                new GetUsersAndUpdateUI().execute();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateUI(List<AccountEntity> items) {
        mAdapter = new UserItemAdapter(items);
        mRecyclerView.setAdapter(mAdapter);
    }

    private class UserItemAdapter extends RecyclerView.Adapter<UserHolder> {
        private List<AccountEntity> mItems;

        public UserItemAdapter(List<AccountEntity> items) {
            this.mItems = items;
        }

        @Override
        public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_user, parent, false);
            return new UserHolder(view);
        }
        @Override
        public void onBindViewHolder(UserHolder holder, int position) {
            AccountEntity item = mItems.get(position);
            holder.bind(item);
        }
        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }

    private class GetUsersAndUpdateUI extends AsyncTask<Void, Void, REST.Result<List<AccountEntity>>> {

        @Override
        protected REST.Result<List<AccountEntity>> doInBackground(Void... params) {
            AccountREST accountREST = new AccountREST();

            REST.Result<List<AccountEntity>> result;

            try {
                List<AccountEntity> accounts = accountREST.getAll();
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, accounts);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<List<AccountEntity>> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                updateUI(result.getResponse());
            } else if (accessResult.equals(REST.AccessResult.UNSUCCESSFUL)){
                Toast.makeText(UserListFragment.this.getActivity(), R.string.access_denied,
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(UserListFragment.this.getActivity(), R.string.cannot_connect,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
