package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.entity.GroupEntity;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Alexey on 15.04.2017.
 */

public class GroupREST extends REST <GroupEntity> {
    public GroupREST() {
        super("groups/");
    }

    public long add(GroupEntity entity) {
        HttpEntity<GroupEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<GroupEntity> response = sRestTemplate.exchange(sURL, HttpMethod.POST, request,
                new ParameterizedTypeReference<GroupEntity>() {});

        return response.getBody().getId();
    }

    public boolean delete(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public GroupEntity get(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<GroupEntity> response = sRestTemplate.exchange(sURL + id, HttpMethod.GET, request,
                new ParameterizedTypeReference<GroupEntity>() {
                });
        return response.getBody();
    }

    public List<GroupEntity> getAll() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<GroupEntity>> response = sRestTemplate.exchange(sURL, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<GroupEntity>>() {
                });
        return response.getBody();
    }

    public boolean update(long id, GroupEntity entity) {
        HttpEntity<GroupEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }
}
