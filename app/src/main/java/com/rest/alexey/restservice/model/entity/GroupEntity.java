package com.rest.alexey.restservice.model.entity;

import java.util.Date;

/**
 * Клас сутності група
 */


public class GroupEntity extends AbstractEntity {

    private String name;
    private int level;
    private String schedule;
    private String trainingMaterials;
    private Date startDate;
    private Date endDate;

    public GroupEntity() {}
    public GroupEntity(final String name, final int level, final String schedule, final String trainingMaterials,
                       final Date startDate, final Date endDate) {
        this.name = name;
        this.level = level;
        this.schedule = schedule;
        this.trainingMaterials = trainingMaterials;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTrainingMaterials() {
        return trainingMaterials;
    }

    public void setTrainingMaterials(String trainingMaterials) {
        this.trainingMaterials = trainingMaterials;
    }
}
