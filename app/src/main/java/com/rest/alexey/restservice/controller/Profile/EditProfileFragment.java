package com.rest.alexey.restservice.controller.Profile;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.AbstractFragment;
import com.rest.alexey.restservice.model.UriWrapper;
import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.FileDTO;
import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.rest.impl.AccountREST;
import com.rest.alexey.restservice.model.rest.impl.FileREST;
import com.rest.alexey.restservice.util.Authorization;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.FileUtils;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class EditProfileFragment extends AbstractFragment {

    private static final int SELECT_IMAGE = 100;
    private EditText mPasswordEditText;

    private ImageView mImageView;

    private Button mEditImageButton;
    private Button mEditButton;

    private AccountEntity mAccountEntity;
    private Uri mImageUri;
    private boolean mImageSelected;
    private FileDTO mFileDTO;

    @Override
    public void onCreate(Bundle savedInstanceBundle) {
        super.onCreate(savedInstanceBundle);

        mAccountEntity = (AccountEntity) getArguments()
                .getSerializable(AccountEntity.class.getName());
        UriWrapper wrapper = (UriWrapper) getArguments().getSerializable("ProfileImage");

        if (wrapper != null) {
            mImageUri = wrapper.getUri();
            getArguments().remove("ProfileImage");
        }
    }

    @Override
    protected void findWidgets(View view) {
        mPasswordEditText = (EditText) view.findViewById(R.id.edit_profile_password_edit_text);

        mImageView = (ImageView) view.findViewById(R.id.edit_profile_image_view);

        mEditImageButton = (Button) view.findViewById(R.id.edit_profile_edit_image_button);
        mEditButton = (Button) view.findViewById(R.id.edit_profile_button);
    }

    @Override
    protected void setUpWidgets() {
        mImageView.setImageURI(mImageUri);

        mEditImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                setImageNotSelected();
            }
        });

        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!mPasswordEditText.getText().toString().equals("")) {
                    mAccountEntity.setPasswordAndEncrypt(mPasswordEditText.getText().toString());
                }

                if (mImageSelected) {
                    mAccountEntity.getUser().setImageName(mFileDTO.getName());
                    new UploadTask(mFileDTO).execute();
                }

                new EditAccount().execute();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_edit_profile;
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, SELECT_IMAGE);
    }

    private void setImageSelected(Uri imageUri) {
        mImageView.setImageURI(imageUri);
        mImageSelected = true;
    }

    private void setImageNotSelected() {
        mImageView.setImageURI(null);
        mImageSelected = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {
            Uri imageUri = data.getData();

            String realPathImage = FileUtils.getRealPathFromURI(imageUri, getActivity());
            File imageFile = new File(realPathImage);

            if (imageFile.exists()) {

                byte[] imageInBytes = null;
                try {
                    imageInBytes = FileUtils.readFile(imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mFileDTO = new FileDTO(DateFormatter.formatWithoutSeparators(new Date()) + ".png", imageInBytes);
                setImageSelected(imageUri);

            } else {
                Toast.makeText(getActivity(), "File doesn't exist", Toast.LENGTH_SHORT).show();
            }
        } else {
            setImageNotSelected();
        }
    }

    private class EditAccount extends AsyncTask<Void, Void, REST.Result<Boolean>> {
        private AccountREST mEntityDAO = new AccountREST();

        @Override
        protected REST.Result<Boolean> doInBackground(Void... params) {
            REST.Result<Boolean> result;

            try {
                Boolean success = mEntityDAO.updateCurrent(mAccountEntity);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, success);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(REST.Result<Boolean> result) {
            REST.AccessResult accessResult = result.getAccessResult();

            if (accessResult.equals(REST.AccessResult.SUCCESSFUL)) {
                Toast.makeText(getActivity(), R.string.edit_toast_successful,
                        Toast.LENGTH_SHORT)
                        .show();
                Authorization.getAuthorization().setAuthorizationData(mAccountEntity);
                GlobalFragmentManager.popBackStack(1, getActivity());
            } else {
                Toast.makeText(getActivity(), R.string.edit_toast_unsuccessful,
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
    private class UploadTask extends AsyncTask<Void, Void, REST.Result<Boolean>> {

        private FileDTO mEntity;
        private FileREST mFileREST = new FileREST();

        public UploadTask(final FileDTO entity) {
            this.mEntity = entity;
        }

        @Override
        protected REST.Result<Boolean> doInBackground(Void... params) {
            REST.Result<Boolean> result;

            try {
                Boolean uploaded = mFileREST.upload(mEntity);
                result = new REST.Result<>(REST.AccessResult.SUCCESSFUL, uploaded);
            } catch (RestClientException exc) {
                if (exc instanceof ResourceAccessException) {
                    result = new REST.Result<>(REST.AccessResult.CANNOT_CONNECT);
                } else {
                    result = new REST.Result<>(REST.AccessResult.UNSUCCESSFUL);
                }
            }
            return result;
        }
    }
}
