package com.rest.alexey.restservice.model;

import com.rest.alexey.restservice.controller.Group.GroupListFragment;
import com.rest.alexey.restservice.controller.User.UserListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Клас для надання фрагментів меню
 */

public class AdminMenuItemProvider {
    private List<AdminMenuItem> adminMenuItems;

    public AdminMenuItemProvider() {
        adminMenuItems = new ArrayList<>();
        adminMenuItems.add(new AdminMenuItem("Users", new UserListFragment()));
        adminMenuItems.add(new AdminMenuItem("Groups", new GroupListFragment()));
    }

    //отримати список пунктів меню
    public List<AdminMenuItem> getAdminMenuItems() {
        return adminMenuItems;
    }
}
