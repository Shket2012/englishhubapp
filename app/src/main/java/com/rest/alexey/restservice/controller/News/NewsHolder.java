package com.rest.alexey.restservice.controller.News;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rest.alexey.restservice.R;
import com.rest.alexey.restservice.controller.MainActivity;
import com.rest.alexey.restservice.model.UriWrapper;
import com.rest.alexey.restservice.model.entity.NewsEntity;
import com.rest.alexey.restservice.util.DateFormatter;
import com.rest.alexey.restservice.util.GlobalFragmentManager;

public class NewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private NewsEntity mNewsEntity;
    private Uri mImagePath;

    private TextView mTitleTextView;
    private TextView mDateTextView;
    private TextView mContentTextView;

    private ImageView mImageView;

    public NewsHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_news_title_text_view);
        mDateTextView = (TextView) itemView.findViewById(R.id.list_item_news_date_text_view);
        mContentTextView = (TextView) itemView.findViewById(R.id.list_item_news_content_text_view);

        mImageView = (ImageView) itemView.findViewById(R.id.list_item_news_image_view);
    }

    public void bindNews(NewsEntity news, Uri imagePath) {
        mNewsEntity = news;

        mTitleTextView.setText(mNewsEntity.getTitle());
        mDateTextView.setText(DateFormatter.formatByDateTime(mNewsEntity.getReleaseDate()));
        mContentTextView.setText(mNewsEntity.getContent());

        mImagePath = imagePath;

        if (mImagePath != null) {
            mImageView.setImageURI(mImagePath);
        }
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(mNewsEntity.getClass().getName(), mNewsEntity);
        bundle.putSerializable("NewsImage", new UriWrapper(mImagePath));

        GlobalFragmentManager.replaceAndAddToBackStackWithBundle(new NewsFragment(),
                bundle, (MainActivity) v.getContext());
    }
}
