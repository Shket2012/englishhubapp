package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.rest.REST;
import com.rest.alexey.restservice.model.entity.UserEntity;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Alexey on 15.04.2017.
 */

public class UserREST extends REST<UserEntity> {
    public UserREST() {
        super("users/");
    }

    public long add(UserEntity entity) {
        HttpEntity<UserEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<UserEntity> response = sRestTemplate.exchange(sURL, HttpMethod.POST, request,
                new ParameterizedTypeReference<UserEntity>() {});

        return response.getBody().getId();
    }

    public boolean delete(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public UserEntity get(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<UserEntity> response = sRestTemplate.exchange(sURL + id, HttpMethod.GET, request,
                new ParameterizedTypeReference<UserEntity>() {
                });
        return response.getBody();
    }

    public List<UserEntity> getAll() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<UserEntity>> response = sRestTemplate.exchange(sURL, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<UserEntity>>() {
                });
        return response.getBody();
    }

    public List<UserEntity> getAllUserByGroup(final long idGroup) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<UserEntity>> response = sRestTemplate.exchange(sURL + "bygroup/" + idGroup, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<UserEntity>>() {
                });
        return response.getBody();
    }

    public UserEntity getCurrentUser() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<UserEntity> response = sRestTemplate.exchange(sURL + "current/", HttpMethod.GET, request,
                new ParameterizedTypeReference<UserEntity>() {
                });
        return response.getBody();
    }

    public boolean update(long id, UserEntity entity) {
        HttpEntity<UserEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }
}
