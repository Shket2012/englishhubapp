package com.rest.alexey.restservice.model.rest.impl;

import com.rest.alexey.restservice.model.entity.MessageEntity;
import com.rest.alexey.restservice.model.rest.REST;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Created by Alexey on 15.04.2017.
 */

public class MessageREST extends REST<MessageEntity> {
    public MessageREST() {
        super("messages/");
    }

    public long add(MessageEntity entity) {
        HttpEntity<MessageEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Long> response = sRestTemplate.exchange(sURL, HttpMethod.POST, request,
                new ParameterizedTypeReference<Long>() {});

        return response.getBody();
    }

    public boolean delete(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.DELETE, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }

    public MessageEntity get(long id) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<MessageEntity> response = sRestTemplate.exchange(sURL + id, HttpMethod.GET, request,
                new ParameterizedTypeReference<MessageEntity>() {
                });
        return response.getBody();
    }

    public List<MessageEntity> getAll() {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<MessageEntity>> response = sRestTemplate.exchange(sURL, HttpMethod.GET, request,
                new ParameterizedTypeReference<List<MessageEntity>>() {
                });
        return response.getBody();
    }

    public List<MessageEntity> getByConversation(long idConversation) {
        HttpEntity request = new HttpEntity(sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<MessageEntity>> response = sRestTemplate.exchange(sURL + "byconversation/" + idConversation,
                HttpMethod.GET, request,
                new ParameterizedTypeReference<List<MessageEntity>>() {
                });
        return response.getBody();
    }

    public List<MessageEntity> getPersonalLastMessages(List<Long> conversationIds) {
        HttpEntity<List<Long>> request = new HttpEntity<>(conversationIds, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<List<MessageEntity>> response = sRestTemplate.exchange(sURL + "last/", HttpMethod.POST, request,
                new ParameterizedTypeReference<List<MessageEntity>>() {
                });
        return response.getBody();
    }

    public boolean update(long id, MessageEntity entity) {
        HttpEntity<MessageEntity> request = new HttpEntity<>(entity, sAuthorization.getHttpHeadersWithAuthData());

        ResponseEntity<Boolean> response = sRestTemplate.exchange(sURL + id, HttpMethod.PUT, request,
                new ParameterizedTypeReference<Boolean>() {
                });
        return response.getBody();
    }
}
