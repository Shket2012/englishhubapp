package com.rest.alexey.restservice.util;

public class AppServerURL {
    private static AppServerURL sAppServerURL;
    private String mURL;

    private AppServerURL() {}

    public static AppServerURL get() {
        if (sAppServerURL == null) {
            sAppServerURL = new AppServerURL();
        }
        return sAppServerURL;
    }

    public void setURL(String url) {
        mURL = url;
    }

    public String getURL() {
        return mURL;
    }
}
