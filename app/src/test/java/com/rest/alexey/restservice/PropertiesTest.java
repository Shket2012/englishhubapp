package com.rest.alexey.restservice;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import static junit.framework.Assert.assertEquals;

/**
 * Клас для тестування Properties
 */

public class PropertiesTest {
    private static final String PROPERTIES_FILENAME = "propertiesEHub.ini";

    //Тестування запису та читання конфігураційних даних з файлу
    @Test
    public void testWriteAndReadProperties() throws Exception {
        Properties properties = new Properties();

        //Додавання даних до буферу
        properties.setProperty("address", "192.168.0.1");
        properties.setProperty("login", "login");
        properties.setProperty("password", "password");

        //Записуємо буфер до файлу
        OutputStream outputStream = new FileOutputStream(PROPERTIES_FILENAME);
        properties.store(outputStream, "What is that?");

        //Зчитуємо дані з файлу
        InputStream inputStream = new FileInputStream(PROPERTIES_FILENAME);
        properties.load(inputStream);

        //Перевіряємо результат зчитування
        assertEquals("192.168.0.1", properties.getProperty("address"));
    }
}
