package com.rest.alexey.restservice;

import com.rest.alexey.restservice.util.DateFormatter;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testFormatForFileName() throws Exception {
        String fileName = DateFormatter.formatWithoutSeparators(new Date());
        System.out.println(fileName);
    }
}