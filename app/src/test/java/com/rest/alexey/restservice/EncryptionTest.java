package com.rest.alexey.restservice;

import com.rest.alexey.restservice.util.Encryption;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Клас для тестування Encription
 */

public class EncryptionTest {

    //Тестування шифрування за допомогою MD5
    @Test
    public void testEncryptMD5() throws Exception {
        //Шифруємо
        String str = Encryption.encryptMD5("test");

        //Повторюємо шифрування та перевіряємо, чи збігається результат
        assertEquals(str, Encryption.encryptMD5("test"));
    }
}
