package com.rest.alexey.restservice;

import android.support.test.runner.AndroidJUnit4;

import com.rest.alexey.restservice.model.entity.AccountEntity;
import com.rest.alexey.restservice.model.entity.FileDTO;
import com.rest.alexey.restservice.model.rest.impl.FileREST;
import com.rest.alexey.restservice.model.rest.impl.MessageREST;
import com.rest.alexey.restservice.util.Authorization;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class UnitTest {

    @BeforeClass
    public static void Init() {
        Authorization authorization = Authorization.getAuthorization();
        authorization.setAuthorizationData(new AccountEntity("admin", "admin"));
    }

    @Test
    public void testGetAllMessages() throws Exception {
        MessageREST messageREST = new MessageREST();
        assertNotNull(messageREST.getAll());
    }

    @Test
    public void testGetLastMessages() throws Exception {
        List<Long> conversations = new ArrayList<>();

        conversations.add(1L);

        MessageREST messageREST = new MessageREST();
        assertNotNull(messageREST.getPersonalLastMessages(conversations));
    }

    @Test
    public void testGetFile() throws Exception {
        FileREST fileREST = new FileREST();

        FileDTO fileDTO = fileREST.download("25052017003928.png");
        assertNotNull(fileDTO);
    }
}
